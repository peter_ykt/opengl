#pragma once
#include "libraries.h"
class RawModel {
private:
	int vaoID;
	int vertexCount;
public:
	RawModel();
	RawModel(int vaoID, int vertexCount);
	GLuint getVaoID() const;
	int getVertexCount();
};