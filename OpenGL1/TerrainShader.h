#pragma once
#include "ShaderProgram.h"
#include "Camera.h"
#include "Light.h"
#include "Maths.h"
class TerrainShader:public ShaderProgram {
private:
	static const char* VERTEX_FILE;
	static const char* FRAGMENT_FILE;
	int location_transformationMatrix;
	int location_projectionMatrix;
	int location_viewMatrix;
	int location_lightPosition;
	int location_lightColour;
	int location_shineDamper;
	int location_reflectivity;
	int location_skyColour;
	int location_backgroundTexture;
	int location_rTexture;
	int location_gTexture;
	int location_bTexture;
	int location_blendMap;
public:
	TerrainShader();
	void loadTransformationMatrix(glm::mat4 matrix);
	void loadProjectionMatrix(glm::mat4 projection);
	void loadViewMatrix(Camera camera);
	void loadLight(Light light);
	void loadShineVariables(float damper, float reflectivity);
	void loadSkyColour(float r, float g, float b);
	void connectTextureUnits();
	//οεπεξοπεδελενθε
protected:
	void bindAttributes();
	void getAllUniformLocations();
};

