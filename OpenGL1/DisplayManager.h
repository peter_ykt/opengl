#pragma once
#include "libraries.h"
#include <Windows.h>
#include <ctime>

using namespace std;


class DisplayManager {
private:
	static const int WIDTH=1200;
	static const int HEIGHT=800;
	static const int FPS_CAP=0.5;
	static int fps;
	static int  lastFrameTime;
	static int  programStart;
	static int  delta;
	static int getCurrentTime();
public:
	//������� ���� ��� ������� ����� ����
	//������� ���� ������������ ��������
	static GLFWwindow* createDisplay();
	static void updateDisplay(GLFWwindow* window);
	static void closeDisplay();
	static int getWidth();
	static int getHeight();
	static int getFrameTimeSeconds();
};


