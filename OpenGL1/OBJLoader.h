#pragma once
#include "RawModel.h"
#include "Loader.h"
#include <sstream>
#include "GLM/glm.hpp"
#include <fstream>
#include <string>
#include <map>
using namespace std;
class OBJLoader
{
private:
	static void processVertex(string vertexData[],vector<int>&indices,
		vector<glm::vec2>&textures,vector<glm::vec3>&normals,float* textureArray,float* normalArray);
	static vector<string>explode(const std::string& str, char delimiter);
	static void indexVBO(
		std::vector<glm::vec3> & in_vertices,
		std::vector<glm::vec2> & in_uvs,
		std::vector<glm::vec3> & in_normals,

		std::vector<unsigned short> & out_indices,
		std::vector<glm::vec3> & out_vertices,
		std::vector<glm::vec2> & out_uvs,
		std::vector<glm::vec3> & out_normals
	);

public:
	static RawModel loadObjModel(const char* fileName, Loader loader);
	OBJLoader();
	~OBJLoader();
};

