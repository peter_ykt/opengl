#pragma once
#include "TexturedModel.h"
#include "GLM\glm.hpp"
#define M_PI 3.141592653f
#define DEG2RAD (M_PI/ 180.0f)
class Entity
{
private:

	TexturedModel model;
	glm::vec3 position=glm::vec3(0.0f,0.0f,0.0f);
	float rotX, rotY, rotZ;
	float scale;
public:
//	Entity();
	Entity(TexturedModel model,glm::vec3 position,float rotX,float rotY,float rotZ,float scale);
	TexturedModel getModel();
	void setModel(TexturedModel model);
	glm::vec3 getPosition();
	void setPosition(glm::vec3 position);
	/*void move() {

	}*/
	float getRotX();
	void setRotX(float rotX);
	float getRotY();
	void setRotY(float rotY);
	float getRotZ();
	void setRotZ(float rotZ);
	float getScale();
	void setScale(float scale);
	void increasePosition(float dx, float dy, float dz);
	void increaseRotation(float dx, float dy, float dz);
	//~Entity();
};

