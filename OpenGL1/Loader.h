#pragma once
#include "libraries.h"
#include "RawModel.h"

class Loader {
public:
	RawModel loadToVAO(float positions[],int arraySize,float texture[],int arraySizeTexture,float normals[],int normalSize,int indices[],int arraySizeIndices);
	void cleanUp();
	GLuint loadTexture(const char*);
private:
	
	GLuint vbos[16];
	GLuint vaos[16];
	GLuint textures[100];
	GLuint createVAO();
	void storeDataInAttributeList(int attributeNumber, int coordinateSize,  float data[],int arraySize);
	void bindIndecesBuffer(int indices[], int arraySize);

	void unbindVAO();
};


