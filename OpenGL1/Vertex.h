#pragma once
#include"GLM\glm.hpp"
static int NO_INDEX = -1;

class Vertex
{

private: 
	glm::vec3 position;
	int textureIndex = NO_INDEX;
	int normalIndex = NO_INDEX;
	Vertex* duplicateVertex=nullptr;
	int index;
	float length;
public:
	Vertex(int index,glm::vec3 position);
	int getIndex();
	float getLength();
	bool isSet();
	
	bool hasSameTextureAndNormal(int textureIndexOther, int normalIndexOther);
	void setTextureIndex(int textureIndex);

	void setNormalIndex(int normalIndex);

	glm::vec3 getPosition();

	int getTextureIndex();
	int getNormalndex();
	Vertex* getDublicateVertex();

	void setupDuplicateVertex(Vertex* duplicateVertex);

	~Vertex();
};

