#include "TerrainTexturePack.h"



TerrainTexturePack::TerrainTexturePack()
{
}

TerrainTexturePack::TerrainTexturePack(TerrainTexture backgroundTexture, TerrainTexture rTexture, TerrainTexture gTexture, TerrainTexture bTexture)
{
	this->backgroundTexture = backgroundTexture;
	this->rTexture = rTexture;
	this->gTexture = gTexture;
	this->bTexture = bTexture;
}

TerrainTexture TerrainTexturePack::getBackgroundTexture()
{
	return backgroundTexture;
}

TerrainTexture TerrainTexturePack::getrTexture()
{
	return rTexture;
}

TerrainTexture TerrainTexturePack::getgTexture()
{
	return gTexture;
}

TerrainTexture TerrainTexturePack::getbTexture()
{
	return bTexture;
}



