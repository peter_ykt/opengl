#include "OBJFileLoader.h"



RawModel OBJFileLoader::loadObj(string objFileName,Loader loader)
{
	vector<glm::vec3> vertices;
	vector<glm::vec2> textures;
	vector<glm::vec3> normals;
	vector<int> indices;
	float* texturesArray = nullptr;
	float* normalsArray = nullptr;
	float* verticesArray = nullptr;
	int* indicesArray = nullptr;

	
	ifstream file(objFileName + ".obj");

	if (!file)
	{
		cout << "File does not open\n\n";
		return RawModel();
	}
	string line;
	while (getline(file, line))
	{
		if (line.substr(0, 2) == "v ")
		{
			//���������� ������ � �����
			istringstream s(line.substr(2));
			//����������� - ������ - ������ �����
			glm::vec3 vertex = glm::vec3();
			s >> vertex.x; s >> vertex.y; s >> vertex.z;
			
			vertices.push_back(vertex);
		}
		else if (line.substr(0, 2) == "vt")
		{
			istringstream s(line.substr(3));
			glm::vec2 texture = glm::vec2();
			s >> texture.x; s >> texture.y;

			textures.push_back(texture);
		}

		else if (line.substr(0, 2) == "vn")
		{
			istringstream s(line.substr(3));
			glm::vec3 normal = glm::vec3();
			s >> normal.x; s >> normal.y; s >> normal.z;

			normals.push_back(normal);
		}

		//������ ��������
		else if (line.substr(0, 2) == "f ")
		{
			texturesArray = new float[vertices.size() * 2];
			normalsArray = new float[vertices.size() * 3];
			break;
		}
	}
	while (line.substr(0, 2) == "f "&& !file.eof())
	{
		if (!(line.substr(0, 2) == "f ")) {
			getline(file, line);
			continue;
		}
		vector<string>currentLine = split(line, ' ');
		vector<string>vertex1 = split(currentLine[1], '/');
		vector<string>vertex2 = split(currentLine[2], '/');
		vector<string>vertex3 = split(currentLine[3], '/');
		processVertex(vertex1, indices, textures, normals, texturesArray, vertices.size() * 2, normalsArray, vertices.size() * 3);
		processVertex(vertex2, indices, textures, normals, texturesArray, vertices.size() * 2, normalsArray, vertices.size() * 3);
		processVertex(vertex3, indices, textures, normals, texturesArray, vertices.size() * 2, normalsArray, vertices.size() * 3);
		
		getline(file, line);
	}
	file.close();
	//removeUnusedVertices(vertices);
	verticesArray = new float[vertices.size() * 3];
	indicesArray = new int[indices.size()];
	int vertexPointer = 0;

	vector<glm::vec3>::iterator ptr;
	for (ptr=vertices.begin();ptr<vertices.end();ptr++)
	{
	
		verticesArray[vertexPointer++] = (*ptr).x;
		verticesArray[vertexPointer++] = (*ptr).y;
		verticesArray[vertexPointer++] = (*ptr).z;
	}
	vertexPointer = 0;
	vector<int>::iterator it;
	for (it = indices.begin(); it < indices.end(); it++)
	{

		indicesArray[vertexPointer++] = (*it);
	
	}

	
	return loader.loadToVAO(verticesArray, vertices.size() * 3, texturesArray,vertices.size() * 2,normalsArray,vertices.size()*3, indicesArray, indices.size());


}

vector<string> OBJFileLoader::split(const string& s, char delimiter)
{
	vector<string> tokens;
	string token;
	istringstream tokenStream(s);
	while (getline(tokenStream, token, delimiter))
	{
		tokens.push_back(token);
	}
	return tokens;
}

void OBJFileLoader::processVertex(vector<string>vertexData, vector<int>& indices,vector<glm::vec2>& textures,vector<glm::vec3>&normals,float* textureArray,int textureArraySize,float* normalsArray,int normalArraySize)
{
	int currentVertexPointer = stoi(vertexData[0]) - 1;
	indices.push_back(currentVertexPointer);
	glm::vec2 currentTex = textures[stoi(vertexData[1]) - 1];
	textureArray[currentVertexPointer * 2] = currentTex.x;
	textureArray[currentVertexPointer * 2+1] = 1-currentTex.y;
	glm::vec3 currentNorm = normals[stoi(vertexData[2]) - 1];
	normalsArray[currentVertexPointer * 3] = currentNorm.x;
	normalsArray[currentVertexPointer * 3+1] = currentNorm.y;
	normalsArray[currentVertexPointer * 3+2] = currentNorm.z;
}

int* OBJFileLoader::convertIndicesListToArray(vector<int> indices, int& arraySize)
{
	int* indicesArray = new int[indices.size()];
	arraySize = indices.size();
	for (int i = 0; i < indices.size(); i++)
	{
		indicesArray[i] = indices[i];
	}
	return indicesArray;
}

float OBJFileLoader::convertDataToArrays(vector<Vertex> vertices, vector<glm::vec2> textures, vector<glm::vec3> normals, float* verticesArray, int verticesArraySize, float* texturesArray, int texturesArraySize, float* normalsArray, int normalsArraySize)
{
	float furthestPoint = 0;
	for (int i = 0; i < vertices.size(); i++)
	{
		Vertex currentVertex = vertices[i];
		if (currentVertex.getLength() > furthestPoint)
		{
			furthestPoint = currentVertex.getLength();
		}
		glm::vec3 position = currentVertex.getPosition();
		glm::vec2 textureCoord = textures[currentVertex.getTextureIndex()];
		glm::vec3 normalVector = normals[currentVertex.getNormalndex()];
		verticesArray[i * 3] = position.x;
		verticesArray[i * 3+1] = position.y;
		verticesArray[i * 3+2] = position.z;
		texturesArray[i * 2] = textureCoord.x;
		texturesArray[i * 2+1] = 1-textureCoord.y;

		normalsArray[i * 3] = normalVector.x;
		normalsArray[i * 3] = normalVector.y;
		normalsArray[i * 3] = normalVector.z;


	}
	return furthestPoint;
}

void OBJFileLoader::dealWithAlreadyProcessedVertex(Vertex * previousVertex, int newTextureIndex, int newNormalIndex, vector<int>& indices, vector<Vertex> vertices)
{
	if (previousVertex != nullptr)
	{
		if (previousVertex->hasSameTextureAndNormal(newTextureIndex, newNormalIndex))
		{
			indices.push_back(previousVertex->getIndex());
		}
		else
		{
			Vertex* anotherVertex = previousVertex->getDublicateVertex();
			if (anotherVertex == nullptr)
			{
				dealWithAlreadyProcessedVertex(anotherVertex, newTextureIndex, newNormalIndex, indices, vertices);
			}
			else {
				Vertex* duplicateVertex = new Vertex(vertices.size(), previousVertex->getPosition());
				duplicateVertex->setTextureIndex(newTextureIndex);
				duplicateVertex->setNormalIndex(newNormalIndex);
				previousVertex->setupDuplicateVertex(duplicateVertex);
				vertices.push_back(*duplicateVertex);
				indices.push_back(duplicateVertex->getIndex());
			}
		}
	}

}

void OBJFileLoader::removeUnusedVertices(vector<Vertex> vertices)
{
	vector<Vertex>::iterator ptr;
	for (ptr = vertices.begin(); ptr < vertices.end(); ptr++)
	{
		if (!(*ptr).isSet()) {
			(*ptr).setTextureIndex(0);
			(*ptr).setNormalIndex(0);
		}
	}
}

OBJFileLoader::OBJFileLoader()
{
}


OBJFileLoader::~OBJFileLoader()
{
}
