#pragma once
#include "StaticShader.h"
#include "Entity.h"
#include <map>
	class EntityRenderer
	{
	public:
	private:
		StaticShader shader;
		void prepareInstance(Entity entity);
		void unbindTexturedModel();
		void prepareTexturedModel(TexturedModel model);
		//отображать внутренние поверхности
		
	public:
		EntityRenderer();
		EntityRenderer(StaticShader shader, glm::mat4 projectionMatrix);
		void render(map<TexturedModel, vector<Entity>>entities);
		static void enableCulling();
		static void disableCulling();

	};
