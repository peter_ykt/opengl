#pragma once
#include "TexturedModel.h"
#include "libraries.h"
#include "TexturedModel.h"
#include "Entity.h"
#include "StaticShader.h"
#include "GLM\glm.hpp"
#include "Maths.h"
#include "DisplayManager.h"
#include <map>
using namespace std;


class Renderer {
private:
	const float FOV = 70;
	const float NEAR_PLANE = 0.1f;
	const float FAR_PLANE = 1000.0f;
	glm::mat4 projectionMatrix;
	StaticShader shader;
	void createProjectionMatrix();
	void prepareInstance(Entity entity);
	void unbindtexturedModel();
	void cleanUp();
public:
	Renderer(StaticShader shader);

	void prepare();

	void render(map<TexturedModel, vector<Entity>>entities);
	void prepareTexturedModel(TexturedModel model);
	void render(Entity entity, StaticShader shader);
};
