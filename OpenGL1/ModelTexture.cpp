#include "ModelTexture.h"

ModelTexture::ModelTexture()
{
	shineDamper = 1;
	reflectivity = 0;
	//hasTransparency = false;
	//useFakeLighting = false;
}

ModelTexture::ModelTexture(int id)
{
	this->texureID = id;
	shineDamper = 1;
	reflectivity = 0;
	//hasTransparency = false;
	//useFakeLighting = false;
}

int ModelTexture::getID() const
{
	return this->texureID;
}

float ModelTexture::getShineDamper()
{
	return shineDamper;
}

void ModelTexture::setShineDamper(float shineDamper)
{
	this->shineDamper = shineDamper;
}

float ModelTexture::getReflectivity()
{
	return reflectivity;
}

void ModelTexture::setReflectivity(float reflectivity)
{
	this->reflectivity = reflectivity;
}

bool ModelTexture::isHasTransparency()
{
	return this->hasTransparency;
}

void ModelTexture::setHasTransparency(bool hasTransparency)
{
	this->hasTransparency = hasTransparency;
}

bool ModelTexture::isUseFakeLighting()
{
	return this->useFakeLighting;
}

void ModelTexture::setUseFakeLighting(bool useFakeLighting)
{
	this->useFakeLighting = useFakeLighting;
}
