#pragma once
#include "RawModel.h"
#include "ModelTexture.h"
#include "ModelData.h"

class TexturedModel
{
public:

	TexturedModel();
	TexturedModel(RawModel model,ModelTexture texture);
	TexturedModel(ModelData model, ModelTexture texture);
	RawModel getRawModel() const;
	ModelTexture* getTexture();
	int getIdItem() const;
	ModelData getModelData() const;
	//����� ��� ����������� ��������� - ��� ��� ������ � ������� map
	//bool TexturedModel::operator==(TexturedModel & b)
	bool operator<(const TexturedModel& rhs) const;
	
private:
	static int idStatic;
	RawModel rawModel;
	ModelTexture texture;
	ModelData modelData;
	int idItem;
};

