#pragma once
#include "GLM\glm.hpp"
#include "GLM\gtc\matrix_transform.hpp"
#include "Camera.h"

class Maths
{
public:
	static glm::mat4 createTransformationMatrix(glm::vec3 translation, float rx, float ry, float rz, float scale);
	static glm::mat4 createTransformation(glm::vec3 translation, float rx, float ry, float rz, float scale);
	static glm::mat4 createViewMatrix(Camera& camera);
	static float getRandom();
	static float barryCentric(glm::vec3 p1, glm::vec3 p2,glm::vec3 p3, glm::vec2 pos);
};

