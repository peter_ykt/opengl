#pragma once
class ModelTexture
{
public:
	ModelTexture();
	ModelTexture(int id);
	int getID() const;
	float getShineDamper();
	void setShineDamper(float shineDamper);
	float getReflectivity();
	void setReflectivity(float reflectivity);
	bool isHasTransparency();
	void setHasTransparency(bool useFakeLighting);
	bool isUseFakeLighting();
	void setUseFakeLighting(bool useFakeLighting);
private:
	int texureID;
	//����� �� ������ ������� �� ���������
	//��������� � ������
	//����� ��������� ������ �� ������
	float shineDamper;
	//����������� ��������� �� �����������
	float reflectivity;
	//��� ������������ ������
	bool hasTransparency=false;
	//����� � ����� ������� �� ����������� ����������
	//��� ����, ����� ���� ������� ���������� ������� ����� ��������� �����
	bool useFakeLighting=false;

};

