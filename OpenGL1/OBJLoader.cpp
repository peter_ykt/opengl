#include "OBJLoader.h"

struct PackedVertex {
	glm::vec3 position;
	glm::vec2 uv;
	glm::vec3 normal;
	bool operator<(const PackedVertex that) const {
		return memcmp((void*)this, (void*)&that, sizeof(PackedVertex)) > 0;
	};
};

bool getSimilarVertexIndex_fast(
	PackedVertex & packed,
	std::map<PackedVertex, unsigned short> & VertexToOutIndex,
	unsigned short & result
) {
	std::map<PackedVertex, unsigned short>::iterator it = VertexToOutIndex.find(packed);
	if (it == VertexToOutIndex.end()) {
		return false;
	}
	else {
		result = it->second;
		return true;
	}
}


void OBJLoader::processVertex(string vertexData[], vector<int>& indices, vector<glm::vec2>& textures, 
	vector<glm::vec3>& normals, float * textureArray, float * normalArray)
{
	//������ ����� ��������� �� ������� ��� ��� ���������� � ����

	int currentVertexPointer = stoi(vertexData[0]) - 1;
	indices.push_back(currentVertexPointer);
	/*int er1 = 0;
	if (stoi(vertexData[2]) > 144)
		er1++;*/
	glm::vec2 currentTex = textures[stoi(vertexData[1]) - 1];
	glm::vec3 currentNorm = normals[stoi(vertexData[2]) - 1];

	textureArray[currentVertexPointer * 2] = currentTex.x;
	textureArray[currentVertexPointer * 2 + 1] = 1 - currentTex.y;

	


	normalArray[currentVertexPointer * 3] = currentNorm.x;
	normalArray[currentVertexPointer * 3 + 1] = currentNorm.y;
	normalArray[currentVertexPointer * 3 + 2] = currentNorm.z;
}

std::vector<std::string> OBJLoader::explode(const std::string& s, char delimiter)
{
	std::vector<std::string> tokens;
	std::string token;
	std::istringstream tokenStream(s);
	while (std::getline(tokenStream, token, delimiter))
	{
		tokens.push_back(token);
	}
	return tokens;
}



//�������� ������
//RawModel OBJLoader::loadObjModel(string fileName, Loader loader)
//{
//	vector<glm::vec3> vertices;
//	vector<glm::vec2> textures;
//	vector<glm::vec3> normals;
//	vector<int> indices;
//	float* textureArray = NULL;
//	float* normalsArray = NULL;
//	float* verticesArray = NULL;
//	int* indicesArray = NULL;
//
//	//std::copy(v.begin(), v.end(), arr);*/
//
//
//
//	//��������� �� ����
//	ifstream file(fileName + ".obj");
//
//	if (!file)
//	{
//		cout << "���� �� ������\n\n";
//		return RawModel();
//	}
//	string line;
//	while (getline(file, line))
//	{
//		if (line.substr(0, 2) == "v ")
//		{
//			//���������� ������ � �����
//			istringstream s(line.substr(2));
//			//����������� - ������ - ������ �����
//			glm::vec3 vertex=glm::vec3();
//			s >> vertex.x; s >> vertex.y; s >> vertex.z;
//			vertices.push_back(vertex);
//		}
//		else if (line.substr(0, 2) == "vn")
//		{
//			istringstream s(line.substr(3));
//			glm::vec3 normal=glm::vec3();
//			s >> normal.x; s >> normal.y; s >> normal.z;
//
//			normals.push_back(normal);
//		}
//		else if (line.substr(0, 2) == "vt")
//		{
//			istringstream s(line.substr(3));
//			glm::vec2 texture=glm::vec2();
//			s >> texture.x; s >> texture.y;
//			
//			textures.push_back(texture);
//		}
//		//������ ��������
//		else if (line.substr(0, 2) == "f ")
//		{
//			//������� ������, ����� ������
//			textureArray = new float[vertices.size()*2];
//			normalsArray = new float[vertices.size()*3];
//	
//			string vertex1[3];
//			string vertex2[3];
//			string vertex3[3];
//			/*	int n = line.length();
//				char* char_array=new char[n + 1];
//				strcpy(char_array, line.c_str());*/
//
//
//				//������ ��������� �� �������
//			vector<string>result = explode(line, ' ');
//			//���������� ������ ������
//			//��� ����������� c 1-�� ����������� ������ ���������� �������
//			//2-�� ����������� ������ ��������
//			//3-�� ����������� ������ ������� 
//			vector<string>vertex1_ = explode(result[1], '/');
//			vector<string>vertex2_ = explode(result[2], '/');
//			vector<string>vertex3_ = explode(result[3], '/');
//
//			//�������� � ������
//			copy(vertex1_.begin(), vertex1_.end(), vertex1);
//			copy(vertex2_.begin(), vertex2_.end(), vertex2);
//			copy(vertex3_.begin(), vertex3_.end(), vertex3);
//
//			processVertex(vertex1, indices, textures, normals, textureArray, normalsArray);
//			processVertex(vertex2, indices, textures, normals, textureArray, normalsArray);
//			processVertex(vertex3, indices, textures, normals, textureArray, normalsArray);
//
//			break;
//
//		}
//
//		
//	}
//	while (getline(file, line))
//	{
//		try {
//			if (line.substr(0, 2) == "f ") {
//				string vertex1[3];
//				string vertex2[3];
//				string vertex3[3];
//				
//
//			   //������ ��������� �� �������
//				vector<string>result = explode(line, ' ');
//				//���������� ������ ������
//				//��� �����������
//				vector<string>vertex1_ = explode(result[1], '/');
//				vector<string>vertex2_ = explode(result[2], '/');
//				vector<string>vertex3_ = explode(result[3], '/');
//				
//				//�������� � ������
//				copy(vertex1_.begin(), vertex1_.end(),vertex1);
//				copy(vertex2_.begin(), vertex2_.end(), vertex2);
//				copy(vertex3_.begin(), vertex3_.end(), vertex3);
//
//				processVertex(vertex1, indices, textures, normals, textureArray, normalsArray);
//				processVertex(vertex2, indices, textures, normals, textureArray, normalsArray);
//				processVertex(vertex3, indices, textures, normals, textureArray, normalsArray);
//				//	delete[] char_array;
//				//	char_array = NULL;
//			}
//		}
//		catch (exception ex)
//		{
//			int ii = 0;
//			ii++;
//		}
//
//	}
//	//�������� �� vector � ������
//	verticesArray=new float[vertices.size()*3];
//	int n = vertices.size();
//	int j = 0;
//	vector<glm::vec3>::iterator ptr;
//	for (ptr=vertices.begin();ptr<vertices.end();ptr++)
//	{
//	
//		verticesArray[j++] = (*ptr).x;
//		verticesArray[j++] = (*ptr).y;
//		verticesArray[j++] = (*ptr).z;
//	}
//	/*for (int i = 0; i < n; i++)
//	{
//		int j = i;
//		verticesArray[j++] = vertices[i].x;
//		verticesArray[j++] = vertices[i].y;
//		verticesArray[j++] = vertices[i].z;
//	}
//*/
//	//�������� �� vector � ������
//	indicesArray = new int[indices.size()];
//	n = indices.size();
//
//
//	for (int i = 0; i < n; i++)
//	{
//		indicesArray[i] = indices[i];
//	
//	}
//
//	/*for (int i = 0; i < indices.size(); i++)
//	{
//		cout << "index coord ";
//		cout<<i + 1;
//		cout << " x:"; 
//		cout << indicesArray[i * 3];
//		cout << endl;
//
//		cout << "index coord ";
//		cout << i + 1;
//		cout << " y:";
//		cout << indicesArray[i * 3+1];
//		cout << endl;
//		
//		cout << "index coord ";
//		cout << i + 1;
//		cout << " z:";
//		cout << indicesArray[i * 3+2];
//		cout << endl;
//
//
//		if (i == 209)
//			break;
//	}*/
//
//	return loader.loadToVAO(verticesArray, vertices.size() * 3, textureArray, vertices.size() * 2, indicesArray, indices.size());
//
//}


RawModel OBJLoader::loadObjModel(
	const char * path,Loader loader) 
{

	std::vector<glm::vec3> out_vertices;
	std::vector<glm::vec2> out_uvs;
	std::vector<glm::vec3> out_normals;

	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;


	FILE * file = fopen(path, "r");
	if (file == NULL) {
		printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
		getchar();

	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				fclose(file);
			
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);

	}




	float* verticesArray = new float[vertexIndices.size() * 3];
	float* textureArray = new float[vertexIndices.size() * 2];
	float* normalsArray = new float[vertexIndices.size() * 3];
	vector<glm::vec3>::iterator ptr3;
	

	std::vector<unsigned short> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;

	indexVBO(out_vertices, out_uvs, out_normals,indices, indexed_vertices, indexed_uvs, indexed_normals);
	int* indicesArray = new int[indices.size()];
	
	for (int i=0;i<indices.size();i++)
	{

		indicesArray[i] = indices[i];
	}

	int j = 0;
	for (ptr3 = indexed_vertices.begin(); ptr3 < indexed_vertices.end(); ptr3++)
	{

		verticesArray[3 * j] = (*ptr3).x;
		verticesArray[3 * j + 1] = (*ptr3).y;
		verticesArray[3 * j + 2] = (*ptr3).z;
		j++;
	}
	vector<glm::vec2>::iterator ptr2;
	j = 0;
	for (ptr2 = indexed_uvs.begin(); ptr2 < indexed_uvs.end(); ptr2++)
	{

		textureArray[2 * j] = (*ptr2).x;
		textureArray[2 * j + 1] = (*ptr2).y;
		j++;
	}
	vector<glm::vec3>::iterator ptr4;
	j = 0;
	for (ptr4 = indexed_normals.begin(); ptr4 < indexed_normals.end(); ptr4++)
	{

		normalsArray[3 * j] = (*ptr4).x;
		normalsArray[3 * j + 1] = (*ptr4).y;
		normalsArray[3 * j + 2] = (*ptr4).z;
		j++;
	}
	
//	float* normalsArray = NULL;
//	float* verticesArray = NULL;
//	int* indicesArray = NULL;


	
	fclose(file);
	return loader.loadToVAO(verticesArray, out_vertices.size()*3, textureArray, out_vertices.size()*2,normalsArray,out_vertices.size()*3, indicesArray, indices.size());
}



void OBJLoader::indexVBO(
	std::vector<glm::vec3> & in_vertices,
	std::vector<glm::vec2> & in_uvs,
	std::vector<glm::vec3> & in_normals,

	std::vector<unsigned short> & out_indices,
	std::vector<glm::vec3> & out_vertices,
	std::vector<glm::vec2> & out_uvs,
	std::vector<glm::vec3> & out_normals
) {
	std::map<PackedVertex, unsigned short> VertexToOutIndex;

	// For each input vertex
	for (unsigned int i = 0; i < in_vertices.size(); i++) {

		PackedVertex packed = { in_vertices[i], in_uvs[i], in_normals[i] };


		// Try to find a similar vertex in out_XXXX
		unsigned short index;
		bool found = getSimilarVertexIndex_fast(packed, VertexToOutIndex, index);

		if (found) { // A similar vertex is already in the VBO, use it instead !
			out_indices.push_back(index);
		}
		else { // If not, it needs to be added in the output data.
			out_vertices.push_back(in_vertices[i]);
			out_uvs.push_back(in_uvs[i]);
			out_normals.push_back(in_normals[i]);
			int newindex = (int)out_vertices.size() - 1;
			out_indices.push_back(newindex);
			VertexToOutIndex[packed] = newindex;
		}
	}
}



OBJLoader::OBJLoader()
{
}


OBJLoader::~OBJLoader()
{
}
