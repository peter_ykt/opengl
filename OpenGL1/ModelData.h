#pragma once
class ModelData
{
private: 
	float* vertices;
	float* textureCoords;
	float* normals;
	int* indices;
	float furthestPoint;
	int verticesSize;
	int textureCoordsSize;
	int normalsSize;
	int indicesSize;
public:
	ModelData();
	ModelData(float* vertices,int verticesSize, float* textureCoords,int textureCoordsSize, 
		float* normals,int normalsSize, int* indices,int indicesSize,
		float furthestPoint);
	float* getVertices() {
		return vertices;
	}
	int getVericesSize() {
		return verticesSize;
	}
	float* getTextureCoords() {
		return textureCoords;
	}
	int getTextureCoordsSize() {
		return textureCoordsSize;
	}
	float* getNormals() {
		return normals;
	}
	int getNormalsSize() {
		return normalsSize;
	}
	int* getIndices() {
		return indices;
	}
	float getFurthestPoint() {
		return furthestPoint;
	}



};

