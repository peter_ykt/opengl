#pragma once
#include "TerrainShader.h"
#include "Terrain.h"
#include "RawModel.h"
class TerrainRenderer
{
private:
	TerrainShader shader;
	void prepareTerrain(Terrain terrain);
	void bindTextures(Terrain terrain);
	void unbindTerrain();
	void loadModelMatrix(Terrain terrain);
public:
	TerrainRenderer();
	TerrainRenderer(TerrainShader shader, glm::mat4 projectionMatrix);
	void render(vector<Terrain> terrains);
};

