#include "Vertex.h"






Vertex::Vertex(int index, glm::vec3 position)
{
	this->index = index;
	this->position = position;
	this->length = glm::length(position);
}


int Vertex::getIndex()
{
	return index;
}

float Vertex::getLength()
{
	return length;
}

bool Vertex::isSet()
{
	return textureIndex != NO_INDEX && normalIndex != NO_INDEX;
}

bool Vertex::hasSameTextureAndNormal(int textureIndexOther, int normalIndexOther)
{
	return textureIndexOther == textureIndex && normalIndexOther == normalIndex;
}

void Vertex::setTextureIndex(int textureIndex)
{
	this->textureIndex = textureIndex;
}

void Vertex::setNormalIndex(int normalIndex)
{
	this->normalIndex = normalIndex;
}

glm::vec3 Vertex::getPosition()
{
	return this->position;
}

int Vertex::getTextureIndex()
{
	return textureIndex;
}

int Vertex::getNormalndex()
{
	return normalIndex;
}

Vertex* Vertex::getDublicateVertex()
{
	return duplicateVertex;
}

void Vertex::setupDuplicateVertex(Vertex* duplicateVertex)
{
	this->duplicateVertex = duplicateVertex;
}

Vertex::~Vertex()
{
}
