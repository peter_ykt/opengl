#include "TerrainShader.h"
const char* TerrainShader::VERTEX_FILE= "terrainVertexShader.txt";
const char* TerrainShader::FRAGMENT_FILE = "terrainFragmentShader.txt";

TerrainShader::TerrainShader():ShaderProgram(TerrainShader::VERTEX_FILE, TerrainShader::FRAGMENT_FILE) {
	//����� ������������ ������ �� ������������ �������� ����������, ������� ����� � ����� ������������
	bindAttributes();
	getAllUniformLocations();
}

void TerrainShader::loadTransformationMatrix(glm::mat4 matrix)
{
	loadMatrix(location_transformationMatrix, matrix);
}

void TerrainShader::loadProjectionMatrix(glm::mat4 projection)
{
	loadMatrix(location_projectionMatrix, projection);
}

void TerrainShader::loadViewMatrix(Camera camera)
{
	glm::mat4 viewMatrix = Maths::createViewMatrix(camera);
	loadMatrix(location_viewMatrix, viewMatrix);
}

void TerrainShader::loadLight(Light light)
{
	loadVector(location_lightPosition, light.getPosition());
	loadVector(location_lightColour, light.getColour());
}

void TerrainShader::loadShineVariables(float damper, float reflectivity)
{
	//��������� � uniform ����������
	loadFloat(location_shineDamper, damper);
	loadFloat(location_reflectivity, reflectivity);
}
void TerrainShader::loadSkyColour(float r, float g, float b)
{
	loadVector(location_skyColour, glm::vec3(r, g, b));
}
void TerrainShader::connectTextureUnits()
{
	loadInt(location_backgroundTexture, 0);
	loadInt(location_rTexture, 1);
	loadInt(location_gTexture, 2);
	loadInt(location_bTexture, 3);
	loadInt(location_blendMap, 4);
}
//��������������� ������������ ������
void TerrainShader::bindAttributes()
{
	bindAttribute(0, "position");
	bindAttribute(1, "textureCoordinates");
	bindAttribute(2, "normal");
}
//��������������� ������������ ������
void TerrainShader::getAllUniformLocations()
{
	//�������� ����� �� ��� � �������
	location_transformationMatrix = getUniformLocation("transformationMatrix");
	location_projectionMatrix = getUniformLocation("projectionMatrix");
	location_viewMatrix = getUniformLocation("viewMatrix");
	location_lightPosition = getUniformLocation("lightPosition");
	location_lightColour = getUniformLocation("lightColour");
	location_shineDamper = getUniformLocation("shineDamper");
	location_reflectivity = getUniformLocation("reflectivity");
	location_skyColour = getUniformLocation("skyColour");
	location_backgroundTexture =getUniformLocation("backgroundTexture");
	location_rTexture = getUniformLocation("rTexture");
	location_gTexture = getUniformLocation("gTexture");
	location_bTexture = getUniformLocation("bTexture");
	location_blendMap = getUniformLocation("blendMap");
}
