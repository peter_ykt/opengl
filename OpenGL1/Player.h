#pragma once
#include "Entity.h"
#include "DisplayManager.h"
#include <math.h>

class Terrain;

class Player:public Entity
{
private:
	GLFWwindow * window;
	static const float RUN_SPEED;
	static const float TURN_SPEED;
	static const float GRAVITY;
	static const float JUMP_POWER;
	static const float TERRAIN_HEIGHT;
	float currentSpeed = 0.0f;
	float currentTurnSpeed = 0.0f;
	float upwardsSpeed = 0;
	bool isAirborn = false;
	void checkInputs();
	void jump();

public:
	/*Player();*/
	static float dx;
	static float dz;
	static float distance;
	Player(GLFWwindow* window,TexturedModel model, glm::vec3 position, float rotX, float rotY, float rotZ, float scale);
	void move(Terrain* terrain);
};

