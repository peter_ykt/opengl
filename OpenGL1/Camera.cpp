
#include "Camera.h"

//GLFW.glfwSetScrollCallback(window, scrollCallback = GLFWScrollCallback((win, dx, dy) ->
//{
//    // Store dx and dy here (both are double values)
//    Mouse.dx = dx;
//    Mouse.dy = dy;
//});


float Camera::mouseWheelMovement = 0.0f;
float Camera::mouseDY = 0.0f;
float Camera::mouseDX = 0.0f;
float Camera::pitchChange = 0.0f;
float Camera::angleAroundPlayer = 0.0f;
double Camera::oldXPos = 0.0f;
double Camera::oldYPos = 0.0f;
float Camera::distanceFromPlayer = 50.0f;
float Camera::pitch = 5.0f;


Camera::Camera(GLFWwindow * window, Player* player)
{
	this->window = window;
	this->player = player;
//	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetScrollCallback(window, scrollCallback);
	glfwSetMouseButtonCallback(window, mouseButtonCallback);
	glfwGetCursorPos(window, &oldXPos, &oldYPos);
}

glm::vec3 Camera::getPosition()
{
	
	return position;

}

float Camera::getPitch()
{
	return pitch;
}

float Camera::getYaw()
{
	return yaw;
}

float Camera::getRoll()
{
	return 0.0f;
}


void Camera::move()
{
	
	float horizontalDistance = calculateHorizontalDistance();
	float verticalDistance = calculateVerticalDistance();
	calculateCameraPosition(horizontalDistance, verticalDistance);
	this->yaw = 180.0f-(player->getRotY()*180/M_PI + angleAroundPlayer*180/M_PI);
	

}

void Camera::calculateCameraPosition(float horizontalDistance, float verticalDistance)
{
	float theta = player->getRotY()+angleAroundPlayer;

	float offsetX = (float)(horizontalDistance * sin(theta));
	float offsetZ = (float)(horizontalDistance * cos(theta));
//	cout << player.getPosition().x << endl;
	position.x = player->getPosition().x - offsetX;
	position.z = player->getPosition().z - offsetZ;
	position.y = player->getPosition().y + verticalDistance + 10;

}

float Camera::calculateHorizontalDistance()
{
	return (float)(distanceFromPlayer * cos(pitch*M_PI/180.0f));
}

float Camera::calculateVerticalDistance()
{
	return (float)(distanceFromPlayer * sin(pitch*M_PI / 180.0f));
}

void Camera::calculateZoom()
{
	float zoomLevel = mouseWheelMovement * 4.0f;
	distanceFromPlayer -= zoomLevel;
}





void Camera::scrollCallback(GLFWwindow*window,double xoffset, double yoffset)
{
	mouseWheelMovement = yoffset;
	float zoomLevel = mouseWheelMovement * 0.2f;
	distanceFromPlayer -= zoomLevel;
}

void Camera::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		double xpos,ypos;
		//getting cursor position
		glfwGetCursorPos(window, &xpos, &ypos);
		float yDelta;
		yDelta = (float)ypos - oldYPos;
		oldYPos = ypos;
		pitchChange = yDelta *2.0f;
		float pitchChangeNow = pitchChange*DEG2RAD;
		pitch -= pitchChangeNow;

		
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		double xpos,ypos;
		//getting cursor position
		glfwGetCursorPos(window, &xpos, &ypos);
		

		float xDelta;
		xDelta = (float)xpos - oldXPos;
		oldXPos = xpos;


		angleAroundPlayer = xDelta *0.003;
		float angleChangeNow = angleAroundPlayer*DEG2RAD;
		angleAroundPlayer -= angleChangeNow;
		//cout << "Cursor Position at (" << xpos << " : " << ypos << endl;
	}

}