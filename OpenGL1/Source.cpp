// GLEW ����� ���������� �� GLFW.
// GLEW
#include "DisplayManager.h"
#include "Loader.h"
#include "Renderer.h"
#include "StaticShader.h"
#include "ModelTexture.h"
#include "TexturedModel.h"
#include "Entity.h"
#include "Camera.h"
#include "libraries.h"
#include "OBJLoader.h"
#include "OBJFileLoader.h"
#include "ModelData.h"
#include "MasterRenderer.h"
#include "Player.h"


//bool operator==(const TexturedModel& t1, const TexturedModel& t2) {
//
//		ModelData modelData1 = t1.getModelData();
//		ModelData modelData2 = t2.getModelData();
//
//		if (modelData1.getTextureCoords() == modelData2.getTextureCoords()
//			&& modelData1.getIndices() == modelData2.getIndices()
//			&& modelData1.getNormals() == modelData2.getNormals()
//			&& modelData1.getVertices() == modelData2.getVertices())
//
//			return true;
//		else
//			return false;
//	}
//typedef std::pair<const TexturedModel, vector<Entity>> PairCompare;
//bool operator==(const PairCompare&p1, const PairCompare&p2) {
//	if (p1.first == p2.first)
//
//		return true;
//	else
//		return false;
//}

int main()
{
	//-------------------------------------------------------------------------------------------------------------------
	//������� ���� ��������� ����� ��� ����������� ������, ������ ����, ��� ��������� ������� �� ����������
	//���������� ��������� �� ��������� (������������ ������ ����)
	//GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "3d game C++", nullptr, nullptr);
	//��������� �������� glfwMakeContextCurrent(window) - ������ ����� �� ������ ������� � API OPENGL;
	GLFWwindow* window=DisplayManager::createDisplay();
	//-------------------------------------------------------------------------------------------------------------------
	glfwMakeContextCurrent(window);

	glewExperimental = true; // ���� ��������� � Core-������ OpenGL
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "���������� ���������������� GLEWn");
		return -1;
	}

	//��������� ������ Loader - ������ ����������� ��� ������
	Loader loader = Loader();
	//���������� ������ � ���������� ������������
	//float vertices[] = {
	//		-0.5f,0.5f,0.0f, //1-� ������� (1-�� ������ �������)
	//		-0.5f,-0.5f,0.0f,//2-� ������� (2-�� ������ �������)
	//		0.5f,-0.5f,0.0f,//3-� ������� (3-�� ������ �������)
	//		0.5f,0.5f,0.0f,

	//		-0.5f,0.5f,1.0f,
	//		-0.5f,-0.5f,1.0f,
	//		0.5f,-0.5f,1.0f,
	//		0.5f,0.5f,1.0f,

	//		0.5f,0.5f,0.0f,
	//		0.5f,-0.5f,0.0f,
	//		0.5f,-0.5f,1.0f,
	//		0.5f,0.5f,1.0f,

	//		-0.5f,0.5f,0.0f,
	//		-0.5f,-0.5f,0.0f,
	//		-0.5f,-0.5f,1.0f,
	//		-0.5f,0.5f,1.0f,

	//		-0.5f,0.5f,1.0f,
	//		-0.5f,0.5f,0.0f,
	//		0.5f,0.5f,0.0f,
	//		0.5f,0.5f,1.0f,

	//		-0.5f,-0.5f,1.0f,
	//		-0.5f,-0.5f,0.0f,
	//		0.5f,-0.5f,0.0f,
	//		0.5f,-0.5f,1.0f

	//};
	//���������� ������� ���������� �� ���� xy(0,1)
	//��������� ��� ����������� ������� � ������������ ������� (��� �� ����������� �� ������������)
	//�� ���� �� ���������� ��������� ������������ �������� ���������� ����������� ��������
	/*float textureCoords[] = {

			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f


	};*/
	//��������� ����� - ��������� ��������� ���������� ������ ��� ������ �������������
	//int indices[] = {
	//		0,1,3, //������� ������ ��� �������������
	//		3,1,2,
	//		4,5,7,
	//		7,5,6,
	//		8,9,11,
	//		11,9,10,
	//		12,13,15,
	//		15,13,14,
	//		16,17,19,
	//		19,17,18,
	//		20,21,23,
	//		23,21,22

	//};
	
	//�������� ������� - ����������� ����� ���������� � ������������ ������� �� ������ (�������� ��������� ��� � ������� glsl)
	//������� ������ ���� ��� ����������� 
	//������������� � ��������� � ��������� ���������, ������������� �� ������� 0 ������� ���������� "position" ���������������� ��� �������
	//�� ������� 1 ������� ������������ ������� "textureCoords" ������� ������������ � ���� ������� glsl 
	//����� ��������������� �������� uniform ���������� ������� ����� ������������ � ��� glsl
	//��� �������� ������ "transformationMatrix","projectionMatrix","viewMatrix"
	//���� ������������ ���� ������ ����������� � ��������� ���������
	//StaticShader shader = StaticShader();
	//��������� � ����������� ���������� uniform ��������� ��������� ������� ProjectionMatrix ������� ������ �� ����������� ���������� �����������
	//����� �������������� glUniform - ������������� uniform ���������� � ��������� ���������, ��� ����� ����� ������������ glUseProgram()
	//Renderer renderer = Renderer(shader);
	//����� ��������� VAO � VBO, ��������� ����� � ���������� ������ � �������� �� ����������� ������ � ������ ����������
	//VAO ������ ������� � ���� VBO
	//VAO ����� ������� ������ ������ �����
	//�������� ���� ��� ������, ���� ��� �������
	//� ����� VAO �������� ��������� �� ����������� VBO,
	//����� �������� � ����� VAO ��� �������� � ��������� 0 � 1 ���� �������� ��������� ����� 
	//��� �������� �������� ������ � � ����� ������� VBO
	//������ ������� VBO ��� ���������� ������ (���������� ����������)
	//������ ������� VBO ��� ���������� ������� (���������� ����������)
	//��������� ����� - ��� ������� ��������
	//������������ ������ ���� RawModel � ���������� VAO � int �������� ���������� �������
//	RawModel model = loader.loadToVAO(vertices,sizeof(vertices)/sizeof(float),textureCoords, sizeof(textureCoords) / sizeof(float),indices,sizeof(indices)/sizeof(int));

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//����������� ����� loadObj ������ OBJFileLoader - ��������� � ������� � ����������� ������ ���������� (��� �����) ���������� ������, �������, ��������, ��������� �����
//����� �������� ���� �������� ������������ � ������� float* textureArray,normalsArray,verticesArray, � int* indicesArray
//����� � ���� ������ ���������� ����� loadToVAO ������ Loader - �� ���������� ������ � �������� �� ����������� ������ � ������ ����������
//����� ��������� VAO � VBO, ��������� ����� � ���������� ������ � �������� �� ����������� ������ � ������ ����������
	//VAO ������ ������� � ���� VBO
	//VAO ����� ������� ������ ������ �����
	//�������� ���� ��� ������, ���� ��� �������
	//� ����� VAO �������� ��������� �� ����������� VBO,
	//����� �������� � ����� VAO ��� �������� � ��������� 0 � 1 ���� �������� ��������� ����� 
	//��� �������� �������� ������ � � ����� ������� VBO
	//������ ������� VBO ��� ���������� ������ (���������� ����������)
	//������ ������� VBO ��� ���������� ������� (���������� ����������)
	//��������� ����� - ��� ������� ��������
	//������������ ������ ���� RawModel � ���������� VAO � int �������� ���������� �������
//	RawModel model = loader.loadToVAO(vertices,sizeof(vertices)/sizeof(float),textureCoords, sizeof(textureCoords) / sizeof(float),indices,sizeof(indices)/sizeof(int));
//������� - ����������� ������ ��������� � ������ ����������, ������ ����� ��������� ��������� ������� ���������� ������

RawModel playerModel= OBJFileLoader::loadObj("player", loader);
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//loader.loadTexture("playerTexture.dds") - ������� �������� �� ����� playerTexture.dds
	//������ �������� �� ����� � �����, �����
	//�� ������ ��������� � ������ ���������� ������� (������ ����������� � ��� ���� ������ ����������� (��������) �� �������� �� ������
	//�������� glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,0, size, buffer + offset); � ����� �� level
	//loader.loadTexture ���������� ������� ��������
	//������ ���� TexturedModel ������� �� �������� RawModel(�������� �������� VAO � ����� ������ ����) �
	//������� �������� � ���������� ��������
TexturedModel playerTextured = TexturedModel(playerModel, loader.loadTexture("playerTexture.dds"));
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//��������� ������ Player - ���������� ��������� window,������ ���� TexturedModel,������ � ��������� ������ ������������ ������� ��������� -100 �� ��� Z, ������ ���������� ��������
//��� ������� move ������������ ������� �� �������, �������� ���� ����������, ��� ���������� �������� �� ������� ������
Player player = Player(window,playerTextured, glm::vec3(100.0f, 0.0f, -50.0f), 0, M_PI, 0, 1);
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   //�������
	RawModel model = OBJFileLoader::loadObj("tree",loader);
	
	TexturedModel tree = TexturedModel(model, loader.loadTexture("tree.dds"));
	//�����
	
	TexturedModel grass = TexturedModel(OBJFileLoader::loadObj("grassModel", loader), ModelTexture(loader.loadTexture("grassTexture.dds")));
	//��� ����� ��� ��� ������ ������� �� ���� ���������� ������� ������ ����������� �����, ����� �� �����������
	grass.getTexture()->setHasTransparency(true);
	grass.getTexture()->setUseFakeLighting(true);
	//����������
	TexturedModel fern= TexturedModel(OBJFileLoader::loadObj("fern", loader), ModelTexture(loader.loadTexture("fernTexture.dds")));
 	fern.getTexture()->setHasTransparency(true);
	//�����
	TexturedModel flower = TexturedModel(OBJFileLoader::loadObj("grassModel", loader), ModelTexture(loader.loadTexture("flower.dds")));
	TexturedModel bobble = TexturedModel(OBJFileLoader::loadObj("lowPolyTree", loader), ModelTexture(loader.loadTexture("lowPolyTree.dds")));
	flower.getTexture()->setHasTransparency(true);
	flower.getTexture()->setUseFakeLighting(true);

	ModelTexture texture = *(tree.getTexture());
	texture.setShineDamper(10);
	texture.setReflectivity(1);
	//������� ����� - ���� �����

	//������ ���� ������ ����� ������ �� ������� ���� ��� ��������� ������� �� ������

		// *********TERRAIN TEXTURE STUFF***********
	TerrainTexture backgroundTexture = TerrainTexture(loader.loadTexture("grass.dds"));
	TerrainTexture rTexture = TerrainTexture(loader.loadTexture("dirt.dds"));
	TerrainTexture gTexture = TerrainTexture(loader.loadTexture("pinkFlowers.dds"));
	TerrainTexture bTexture = TerrainTexture(loader.loadTexture("path.dds"));
	TerrainTexturePack texturePack = TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
	TerrainTexture blendMap = TerrainTexture(loader.loadTexture("blendMap.dds"));
	Terrain terrain = Terrain(0, -1, loader, texturePack, blendMap,"heightmap.png");
	Terrain terrain2 = Terrain(1, 0, loader, texturePack, blendMap,"heightmap.png");
	// *****************************************
	vector<Entity> entities;
	

	for (int i = 0; i < 100; i++)
	{
		if (i % 7 == 0) {
			float x = Maths::getRandom() * 1000 - 400;
			if (x < 0)
				x = (-1)*x;
			float z = Maths::getRandom() * -800;
			if (z > 0)
				z = (-1)*z;
			float y = terrain.getHeightOfTerrain(x, z);
			////��������� �������� � ������� ����������� �� ���������� ������������
			entities.push_back(Entity(fern, glm::vec3(x, y, z), 0, 0, 0, 0.9f));
			entities.push_back(Entity(grass, glm::vec3(x, y, z), 0, 0, 0, 1.0f));
			entities.push_back(Entity(flower, glm::vec3(x, y, z), 0, 0, 0, 1.0f));
			x = Maths::getRandom() * 1000 - 400;
			if (x < 0)
				x = (-1)*x;
			z = Maths::getRandom() * -800;
			if (z > 0)
				z = (-1)*z;
			y = terrain.getHeightOfTerrain(x, z);
			/*x = 400;
			z = -400;*/
			y = terrain.getHeightOfTerrain(x, z);
			entities.push_back(Entity(bobble, glm::vec3(x, y, z), 0, 0, 0, 1.0f));
			x = Maths::getRandom() * 1000 - 400;
			if (x < 0)
				x = (-1)*x;
			z = Maths::getRandom() * -800;
			if (z > 0)
				z = (-1)*z;
			y = terrain.getHeightOfTerrain(x, z);
			entities.push_back(Entity(tree, glm::vec3(x, y, z), 0, 0, 0, 10));
		}
			
		//if (i % 3==0)
		//{
		//	float x = Maths::getRandom() * 800 - 400;
		//	float z = Maths::getRandom() * -600;
		//	float y = terrain.getHeightOfTerrain(x, z);
		//	entities.push_back(Entity(tree, glm::vec3(x,y,z), 0, 0, 0, 10));
		//	entities.push_back(Entity(flower, glm::vec3(x,y,z), 0, 0, 0, 1.0f));
		//}
		/*if (i % 2==0)
		{
			float x = Maths::getRandom() * 800 - 400;
			float z = Maths::getRandom() * -600;
			float y = terrain.getHeightOfTerrain(x, z);
			entities.push_back(Entity(fern, glm::vec3(x, y, z), 0, 0, 0, 0.9f));
		}*/
		
	}
	//���� �����
	Light light = Light(glm::vec3(20000.0f, 20000.0f, 2000.0f), glm::vec3(1, 1, 1));
	//������� ������ ������ - ����������� ������ ������� �� ����������� ������
	//������ ������������ ������� ����
	Camera camera = Camera(window,&player);
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//��� �������� MasterRenderer � ���� ���� ��������� ��������
	//StaticShader shader = StaticShader(); ������� ����� ������� ������
	//�������� ������� - ����������� ����� ���������� � ������������ ������� �� ������ (�������� ��������� ��� � ������� glsl)
	//������� ������ ���� ��� ����������� 
	//������������� � ��������� � ��������� ���������, ������������� �� ������� 0 ������� ���������� "position" ���������������� ��� �������
	//�� ������� 1 ������� ������������ ������� "textureCoords" ������� ������������ � ���� ������� glsl 
	//����� ��������������� �������� uniform ���������� ������� ����� ������������ � ��� glsl
	//��� �������� ������ "transformationMatrix","projectionMatrix","viewMatrix"
	//"transformationMatrix" - ��� ������� ������
	//������� ������ � ������� ���� ���������� ���������� ��������� ������� ������ �� �������, ����� ������� ��������, ����� �� ������� ��������
    //���� ������������ ���� ������ ����������� � ��������� ���������
	//StaticShader shader = StaticShader();
	//��������� � ����������� ���������� uniform ��������� ��������� ������� ProjectionMatrix ������� ������ �� ����������� ���������� �����������
	//����� �������������� glUniform - ������������� uniform ���������� � ��������� ���������, ��� ����� ����� ������������ glUseProgram()
	//viewMatrix ��� ��� �� ������� ������
	/*glm::mat4 Maths::createViewMatrix(Camera& camera)*/
	//{
	//	//��������� �������
	//	glm::mat4 viewMatrix = glm::mat4(1.f);
	//	//viewMatrix = glm::rotate(viewMatrix,camera.getPitch()*M_PI/180.0f, glm::vec3(1, 0, 0));

	//	viewMatrix = glm::rotate(viewMatrix, camera.getPitch()*M_PI / 180.0f, glm::vec3(1, 0, 0));
	//	viewMatrix = glm::rotate(viewMatrix, camera.getYaw()*M_PI / 180.0f, glm::vec3(0, 1, 0));
	//	glm::vec3 cameraPos = camera.getPosition();
	//	//������� ������ �� ������������� ������� �� ���� (���������� ��� ���)
	//	glm::vec3 negativeCameraPos = glm::vec3(-cameraPos.x, -cameraPos.y, -cameraPos.z);
	//	viewMatrix = glm::translate(viewMatrix, negativeCameraPos);
	//	return viewMatrix;

	//}

	//������� ������� �������� - frustrum
	MasterRenderer renderer = MasterRenderer();
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//��������� ������ ���� Entity �������� �������� ������ ���� TextureModel � ������������ �������, 
	//���������� ������ �������� � ����������������
	//Entity entity = Entity(staticModel, glm::vec3(0, 0, -50.0f),0,0,0,1);

	
	//��������� ������� �����
	glEnable(GL_DEPTH_TEST);
	/*int nbFrames = 0;
	double lastTime = glfwGetTime();*/

	
	while (!glfwWindowShouldClose(window))
	{	
    	//������� ��������� ������� ���� (��������� � position) � ����������� �� ������� �� ������ 
		//(������ ����� ��� 0.02 ��� ������ �������)

	//	int ms_start = GetTickCount();
		    player.move(&terrain);
			camera.move();
	
			

			renderer.processEntity(player);
//	
			//����������� �������� ���� �� ���� X � Y 
			renderer.processTerrain(terrain);

			renderer.processTerrain(terrain2);
//			DisplayManager::updateDisplay(window);
			for (int i = 0; i < entities.size(); i++)
			{
				renderer.processEntity(entities[i]);
	//			DisplayManager::updateDisplay(window);
			}
			//entity.increaseRotation(0.0f, 0.05f, 0.0f);
			//������� ������,������� ������� �������
		//	renderer.prepare();
			// ������ glUseProgram(programID) - ������ ��� ���������� �������
		//	shader.start();
	//		shader.loadLight(light);
			//����������� ������� view ������ - ��������� ���������� ��������� �������
			//� ���������� �� ������� �������� �� ����,
			//����� ���������� �� ������� �������� (������� �������� � ������ �������� �� -position.x,-position.y,-position.z)
			//� ����� ���������� ������� view ������� �������������� ���������� uniform ������� viewMatrix
	//		shader.loadViewMatrix(camera);
			//������������ VAO, ��������������� �������� ��� ����������
			//������� ������� - ������� � ������� ��������� �� ������ ������� ���������
			//���������� ������� ������ ���������� �� ������ ������ - ����� ������� ���������� ������
			//�� ������� ������� (������ ���������)
			//����� �������� �� �������� ����� ��������
			//glActiveTexture(GL_TEXTURE0);
			//glBindTexture(GL_TEXTURE_2D, model.getTexture().getID());
			//� �������� ������
			//glDrawElements(GL_TRIANGLES, rawModel.getVertexCount(), GL_UNSIGNED_INT, 0);

			//double currentTime = glfwGetTime();
			//nbFrames++;
			//if (currentTime - lastTime >= 1.0) { // If last prinf() was more than 1sec ago
			//	// printf and reset
			//	printf("%f ms/frame\n", 1000.0 / double(nbFrames));
			//	nbFrames = 0;
			//	lastTime += 1.0;
			//}
		
			renderer.render(light, camera);
			DisplayManager::updateDisplay(window);
			//int ms_end = GetTickCount();
	         
		//	cout << ms_end - ms_start << endl;
		//��������� ��� �����������
//		shader.stop();

	
	
	//	glfwSwapBuffers(window);
	//	glfwPollEvents();
	}
//	shader.cleanUp();
	renderer.cleanUp();
   loader.cleanUp();

	DisplayManager::closeDisplay();


	return 0;
}