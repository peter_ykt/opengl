#pragma once
#include "TerrainTexture.h"
class TerrainTexturePack
{
private:
	TerrainTexture backgroundTexture;
	TerrainTexture rTexture;
	TerrainTexture gTexture;
	TerrainTexture bTexture;
public:
	TerrainTexturePack();
	TerrainTexturePack(TerrainTexture backgroundTexture, TerrainTexture rTexture,TerrainTexture gTexture,TerrainTexture bTexture);
	TerrainTexture getBackgroundTexture();
	TerrainTexture getrTexture();
	TerrainTexture getgTexture();
	TerrainTexture getbTexture();
};

