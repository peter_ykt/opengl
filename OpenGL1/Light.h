#pragma once
#include "GLM\glm.hpp"
class Light
{
private:
	glm::vec3 position;
	glm::vec3 colour;
public:
	Light();
	Light(glm::vec3 position, glm::vec3 colour);
	glm::vec3 getPosition();
	void setPosition(glm::vec3 position);
	glm::vec3 getColour();
	void setColour(glm::vec3 colour);
	~Light();
};

