#pragma once
#include "libraries.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include "GLM/glm.hpp"
#include "GLM/gtc/type_ptr.hpp"
using namespace std;
class ShaderProgram {
private:
	int programID;
	int vertexShaderID;
	int fragmentShaderID;
	int loadShader(const char* file, int type);
public: 
	ShaderProgram(const char* vertexFile, const char* fragmentFile);
	void start();
	void stop();
	void cleanUp();
protected:
	
	virtual void bindAttributes()=0;
	virtual void getAllUniformLocations()=0;
	void bindAttribute(int attribute, const char* variableName);
	int getUniformLocation(const char* uniformName);
	void loadFloat(int location, float value);
	void loadInt(int location, int value);
	void loadVector(int location, glm::vec3 vector);
	void loadBoolean(int location, bool value);
	void loadMatrix(int location, glm::mat4 matrix);
};
