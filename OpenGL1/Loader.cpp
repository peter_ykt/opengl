#include "Loader.h"
#include "libraries.h"
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

RawModel Loader::loadToVAO(float positions[],int arraySize,float texture[],int arraySizeTexture, float normals[], int normalSize,int indices[],int arraySizeIndices)
{

	//VAO ������ ������� � ���� VBO
	//VAO ����� ������� ������ ������ �����
	//�������� ���� ��� ������, ���� ��� �������
	//� ����� VAO �������� ��������� �� ����������� VBO,
	//����� �������� � ����� VAO ��� �������� � ��������� 0 � 1 ���� �������� ��������� ����� 
	//��� �������� �������� ������ � � ����� ������� VBO
	//������ ������� VBO ��� ���������� ������ (���������� ����������)
	//������ ������� VBO ��� ���������� ������� (���������� ����������)
	//��������� ����� - ��� ������� ��������
	GLuint vaoID = createVAO();
	bindIndecesBuffer(indices, arraySizeIndices);
	//������ �������� ��� ������ ��������
	storeDataInAttributeList(0, 3,positions,arraySize); //3 ���������� ��� ������
	storeDataInAttributeList(1, 2, texture, arraySizeTexture); //2 ���������� ��� �������
	storeDataInAttributeList(2, 3, normals, normalSize); //3 ���������� ��� ��������
	unbindVAO();
	//������ ����� ������� ����� �� ������ ������ ��������
	return RawModel(vaoID, arraySizeIndices);
}

void Loader::cleanUp()
{
	

	glDeleteVertexArrays(sizeof(vaos)/sizeof(GLuint), &vaos[0]);
	glDeleteBuffers(sizeof(vbos) / sizeof(GLuint), &vbos[0]);
	glDeleteTextures(sizeof(textures) / sizeof(GLuint), &textures[0]);

}

//� ������ ���������� �������� ����� �������� � ��������� ��������, ��������, 
//RGB8, RGBA8 ��� R3G3B2.� ������ ������ ���� ������ �������� ��� �����, �� ������ � ������, � ������� � ���� ����.
GLuint Loader::loadTexture(const char * imagepath) {

	unsigned char header[124];
	//��������� ����������� ���� � ������ � buffer
	FILE *fp;

	/* try to open the file */
	fp = fopen(imagepath, "rb");
	if (fp == NULL) {
		printf("%s Can not open file. A you sure about current directory? Do not forgte read FAQ!\n", imagepath); getchar();
		return 0;
	}

	/* verify the type of file */
	char filecode[4];
	fread(filecode, 1, 4, fp);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);

	if (strncmp(filecode, "DDS ", 4) != 0) {
		fclose(fp);
		return 0;
	}

	/* get the surface desc */
	fread(&header, 124, 1, fp);

	unsigned int height = *(unsigned int*)&(header[8]);
	unsigned int width = *(unsigned int*)&(header[12]);
	unsigned int linearSize = *(unsigned int*)&(header[16]);
	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
	unsigned int fourCC = *(unsigned int*)&(header[80]);


	unsigned char * buffer;
	unsigned int bufsize;
	/* how big is it going to be including all mipmaps? */
	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
	//����������� ����� � ����������� ������
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
	fread(buffer, 1, bufsize, fp);
	/* close the file pointer */
	fclose(fp);

	unsigned int components = (fourCC == FOURCC_DXT1) ? 3 : 4;
	unsigned int format;
	switch (fourCC)
	{
	case FOURCC_DXT1:
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		break;
	case FOURCC_DXT3:
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		break;
	case FOURCC_DXT5:
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		break;
	default:
		free(buffer);
		return 0;
	}

	// 
	// 
	
	//	������� ��� ��������
	//	������� ���� �������� � ������
	//	���������� ��������� ��������
	//	���������� ��������� �������������� �������� � ��������
	//	������� ���������� �������� � ��������
	GLuint textureID;
	//	������� ���� �������� � ������� ��� - ������������� ��������
	glGenTextures(1, &textureID);

	// ���������� �������� � ���������� ��������
	// ������� ��������� �������� �������
	//��� ����� ������ ������� glBindTexture.������ �������� ������ ���� GL_TEXTURE_2D ��� GL_TEXTURE_1D.
	//�� ����������, � ���������� ��� ��������� ������������ ����� ��������.
	glBindTexture(GL_TEXTURE_2D, textureID);
	//����� ������� glPixelStorei � ������, ��� ������������ � ������� ������ ���� �� �����.
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
	unsigned int offset = 0;
	//�������� �������� (������� ��� ����� �����������-������� ��� ������ ����������� �������� ����� ������ �������)
	/* �������� �������� */
	for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
	{
		unsigned int size = ((width + 3) / 4)*((height + 3) / 4)*blockSize;
		//������ �� ������ ������� ���� �������� � ������.
		//������ ��� � �������� ����� ��������� ����������.
		//������ ��������, �� ������� �� ������������� ����������.
		//����� ���������� �������� �� ���������� ������� �����������, ������ � ������
		//���������� �������� � ��������.
		//������� ����������� ����� ��� ��������� �������� �� ������� �������, �.�.����� ������� �� ������ ������ �������� �����������.
		//����������� � ������ ����������

		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
			0, size, buffer + offset);

		offset += size;
		width /= 2;
		height /= 2;

		// Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
		if (width < 1) width = 1;
		if (height < 1) height = 1;

	}
	//����������� �����
	free(buffer);

	return textureID;


}



GLuint Loader::createVAO()
{
//VAO - ��� ����� �����, ������� ������� OpenGL, 
//����� ����� VBO ������� ������������ � ����������� ��������.
//����� ���� ��������, �����������, ��� VAO ������������ ����� ������, 
//� ��������� �������� �������� ���������� � ���, ����� ����� ������ VBO ������������, 
//� ��� ��� ������ ����� ����������������
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	vaos[vaoID]=vaoID;
	glBindVertexArray(vaoID);
	return vaoID;
}
//��������� ������ 
void Loader::storeDataInAttributeList(int attributeNumber,int coordinateSize, float data[],int arraySize)
{
	GLuint vboID;
	//���������� �������� ���������� �������������� � ��������� ����� ���������� VBO 1 - ��� ����� �������
	glGenBuffers(1, &vboID);
	vbos[vboID]=vboID;
	//�� ����� ��������� � ������ ���������� ������ � ��� ����� ���� ����� ���������� GL_ARRAY_BUFFER
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	//���������� ������ � ������ ���������� �� ����������� ������ � ������ ����������
	glBufferData(GL_ARRAY_BUFFER, arraySize*sizeof(float),&data[0], GL_STATIC_DRAW);
	//��������� ������ ����� ������ ��� ������� ��������� (������ �������� ����� ��� ��������)
	//������ �������� ������ �����������
	glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, GL_FALSE, 0, nullptr);

	//������ ������ ��������� � ����������� � ����� ������� ������ �� ����������� ������
	glBindBuffer(GL_ARRAY_BUFFER,0);



}

//����� ��� ���������� ����� ������ - ���������� ������
void Loader::bindIndecesBuffer(int indices[], int arraySize)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos[vboID] = vboID;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, arraySize * sizeof(int), &indices[0], GL_STATIC_DRAW);

}

void Loader::unbindVAO()
{
	glBindVertexArray(0);

}


