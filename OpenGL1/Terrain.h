#pragma once
#include "RawModel.h"
#include "ModelTexture.h"
#include "Loader.h"
#include "TerrainTexture.h"
#include "TerrainTexturePack.h"
#include "LibPNG\png.h"
#include "Maths.h"
#include <math.h>
class Terrain
{
private:
	static const float SIZE;
	//����� ����� �� ������ �������
	static const int VERTEX_COUNT;
	float x;
	float z;
	int width, height;
	png_byte color_type;
	png_byte bit_depth;
	png_bytep *row_pointers = NULL;
	const float MAX_HEIGHT = 40;
	const float MAX_PIXEL_COLOR = 256 * 256 * 256;

	RawModel model;
	TerrainTexturePack texturePack;
	TerrainTexture blendMap;
	RawModel generateTerrain(Loader loader, const char * heightMap);
	glm::vec3 calculateNormal(int x, int z);
	float ** heights;
public:
	Terrain(int gridX,int gridZ,Loader loader,TerrainTexturePack texturePack, TerrainTexture blendMap,const char * heightMap);

	float getX();
	float getZ();
	RawModel getModel();
	TerrainTexturePack getTexturePack();
	TerrainTexture getBlendMap();
	void read_png_file(const char *filename);
	float getHeight(int x, int y);
	float getHeightOfTerrain(float worldX, float worldZ); 
};

