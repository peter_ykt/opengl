#define DEG2RAD (M_PI/ 180.0f);
#pragma once
#include "GLM\glm.hpp"
#include "libraries.h"
#include "Player.h"
#include <math.h>

class Camera {
private:
	GLFWwindow* window;
	Player* player;
	glm::vec3 position = glm::vec3(0, 0, 150.0f);
	float yaw=0;
	float roll;
	static double oldXPos;
	static double oldYPos;
	static float mouseWheelMovement;
	static float mouseDY;
	static float mouseDX;
	static float distanceFromPlayer;

	//float pitch = 30.0f/M_PI;
	static float pitchChange;
	static float angleAroundPlayer;
	static float pitch;
	void calculateCameraPosition(float horizontalDistance, float verticalDistance);
	float calculateHorizontalDistance();
	float calculateVerticalDistance();
	void calculateZoom();
	/*void calculatePitch();*/
	/*void calculateAngleAroundPlayer();*/
	// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
	static void scrollCallback(GLFWwindow* window,double xoffset, double yoffset);
	static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	
public: 
	Camera(GLFWwindow* window,Player* player);
	glm::vec3 getPosition();
	float getPitch();
	float getYaw();
	float getRoll();
	void move();



};
