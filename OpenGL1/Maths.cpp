#include "Maths.h"

glm::mat4 Maths::createTransformationMatrix(glm::vec3 translation, float rx, float ry, float rz, float scale)
{
	glm::mat4 matrix = glm::mat4(1.f);
	//�������
	matrix = glm::translate(matrix, translation);
	//��������

	matrix = glm::rotate(matrix, rx, glm::vec3(1, 0, 0));
	matrix = glm::rotate(matrix, ry, glm::vec3(0, 1, 0));
	matrix = glm::rotate(matrix, rz, glm::vec3(0, 0, 1));
	matrix = glm::scale(matrix, glm::vec3(scale, scale, scale));
	return matrix;
}

glm::mat4 Maths::createTransformation(glm::vec3 translation, float rx, float ry, float rz, float scale) {
	glm::mat4 matrix = glm::mat4(1.f);
	//�������
	matrix=glm::translate(matrix, translation);
	//��������

	matrix = glm::rotate(matrix, rx , glm::vec3(1, 0, 0));
	matrix = glm::rotate(matrix, ry, glm::vec3(0, 1, 0));
	matrix = glm::rotate(matrix, rz, glm::vec3(0, 0, 1));
	matrix = glm::scale(matrix, glm::vec3(scale, scale, scale));
	return matrix;
}

//view ������� - ��� ��� ����� ���� �� ������� ������
glm::mat4 Maths::createViewMatrix(Camera& camera)
{ 
	//��������� �������
	glm::mat4 viewMatrix = glm::mat4(1.f);
	//viewMatrix = glm::rotate(viewMatrix,camera.getPitch()*M_PI/180.0f, glm::vec3(1, 0, 0));

	viewMatrix = glm::rotate(viewMatrix,camera.getPitch()*M_PI/180.0f, glm::vec3(1, 0, 0));
	//viewMatrix = glm::rotate(viewMatrix, camera.getPitch()*M_PI / 180.0f, glm::vec3(1, 0, 0));
	viewMatrix = glm::rotate(viewMatrix, camera.getYaw()*M_PI/180.0f, glm::vec3(0, 1, 0));
	glm::vec3 cameraPos = camera.getPosition();
	//������� ������ �� ������������� ������� �� ���� (���������� ��� ���)
	glm::vec3 negativeCameraPos = glm::vec3(-cameraPos.x, -cameraPos.y, -cameraPos.z);
	viewMatrix = glm::translate(viewMatrix, negativeCameraPos);
	return viewMatrix;

}

float Maths::getRandom()
{
	float res= (rand() % 100) / (100 * 1.0);
	if (res > 0)
		return res;
	else

		return (-1)*res;
}

float Maths::barryCentric(glm::vec3 p1, glm::vec3 p2,glm::vec3 p3, glm::vec2 pos)
{

		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
		float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
		float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
		float l3 = 1.0f - l1 - l2;
		return l1 * p1.y + l2 * p2.y + l3 * p3.y;
	
}


