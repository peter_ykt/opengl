
#include "ShaderProgram.h"

using namespace std;

int ShaderProgram::loadShader(const char* file, int type)
{


	string shaderSource;
	ifstream fileStream(file, std::ios::in);

	if (!fileStream.is_open()) {
		cout << "Can not open file " << file << ". File does not exist." << endl;
		shaderSource = "";
	}
	GLint result = GL_FALSE;
	int logLength;
	string line = "";
	while (!fileStream.eof()) {
		getline(fileStream, line);
		shaderSource.append(line + "\n");
	}

	fileStream.close();

	GLuint shaderID = glCreateShader(type);
	const char* shaderStr = shaderSource.c_str();
	
	glShaderSource(shaderID,1,&shaderStr,NULL);

	glCompileShader(shaderID);

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<GLchar> errorLog(logLength);
		errorLog[0] = (logLength > 1) ? logLength : 1;
	
		glGetShaderInfoLog(shaderID, logLength, &logLength, &errorLog[0]);
		for(int i=0;i<logLength;i++)
		cout << errorLog[i];
		cout<<"Can not complie shader!"<<endl;
		return -1;
	}
	return shaderID;
}

ShaderProgram::ShaderProgram(const char* vertexFile, const char* fragmentFile)
{
	vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
	fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);
	glValidateProgram(programID);

}

void ShaderProgram::start()
{
	glUseProgram(programID);
}

void ShaderProgram::stop()
{
	glUseProgram(0);
}

void ShaderProgram::cleanUp()
{
	stop();
	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);
	glDeleteProgram(programID);
	//getAllUniformLocations();
}

void ShaderProgram::bindAttributes()
{
}

void ShaderProgram::bindAttribute(int attribute, const char* variableName)
{
	glBindAttribLocation(programID, attribute, variableName);
}

int ShaderProgram::getUniformLocation(const char* uniformName)
{
	return glGetUniformLocation(programID, uniformName);
}

void ShaderProgram::getAllUniformLocations() {};


void ShaderProgram::loadFloat(int location, float value)
{
	glUniform1f(location, value);
}

void ShaderProgram::loadInt(int location, int value)
{
	glUniform1i(location, value);
}

void ShaderProgram::loadVector(int location, glm::vec3 vector)
{
	glUniform3f(location, vector.x, vector.y, vector.z);
}

void ShaderProgram::loadBoolean(int location, bool value)
{
	float toLoad = 0;
		if (value) {
			toLoad = 1;
		}
	glUniform1f(location, toLoad);
}

void ShaderProgram::loadMatrix(int location, glm::mat4 matrix)
{
	
	glUniformMatrix4fv(location, 1, GL_FALSE,&matrix[0][0]);
	/*cout << "matrix[0][0]="<<matrix[0][0] << endl;
	cout << "matrix[0][1]=" << matrix[0][1] << endl;
	cout << "matrix[0][2]=" << matrix[0][2] << endl;
	cout << "matrix[0][3]=" << matrix[0][3] << endl;
	cout << "matrix[1][0]=" << matrix[1][0] << endl;
	cout << "matrix[1][1]=" << matrix[1][1] << endl;
	cout << "matrix[1][2]=" << matrix[1][2] << endl;
	cout << "matrix[1][3]=" << matrix[1][3] << endl;
	cout << "matrix[2][0]=" << matrix[2][0] << endl;
	cout << "matrix[2][1]=" << matrix[2][1] << endl;
	cout << "matrix[2][2]=" << matrix[2][2] << endl;
	cout << "matrix[2][3]=" << matrix[2][3] << endl;
	cout << "matrix[3][0]=" << matrix[3][0] << endl;
	cout << "matrix[3][1]=" << matrix[3][1] << endl;
	cout << "matrix[3][2]=" << matrix[3][2] << endl;
	cout << "matrix[3][3]=" << matrix[3][3] << endl;*/
	
}
