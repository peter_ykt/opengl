#include "Light.h"



Light::Light()
{
}

Light::Light(glm::vec3 position, glm::vec3 colour)
{
	this->position = position;
	this->colour = colour;
}

glm::vec3 Light::getPosition()
{
	return position;
}

void Light::setPosition(glm::vec3 position)
{
	this->position = position;
}

glm::vec3 Light::getColour()
{
	return colour;
}

void Light::setColour(glm::vec3 colour)
{
	this->colour = colour;
}


Light::~Light()
{
}
