#include "DisplayManager.h"

int DisplayManager::programStart=0;
int DisplayManager::lastFrameTime;
int DisplayManager::delta=0;
int DisplayManager::fps = 0;

int DisplayManager::getCurrentTime()
{
	//����� ������ � 1970 ����
	DisplayManager::programStart = GetTickCount();
//	DisplayManager::programStart = clock();
	return DisplayManager::programStart;
}

GLFWwindow* DisplayManager::createDisplay() {
		//������������� GLFW - ����� ������, ������� � 1970 ����
	//	DisplayManager().getCurrentTime();
		//������������� ���������� GLFW
		glfwInit();
		//��������� GLFW
		//�������� ����������� ��������� ������ OpenGL. 
		//�������� 
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		//��������
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		//��������� �������� ��� �������� ��������� ��������
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		//���������� ����������� ��������� ������� ����
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		//���������� ��������� �� ��������� (������������ ������ ����)
		GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "3d game C++", nullptr, nullptr);
		if (window == nullptr)
		{
			std::cout << "Error creating FLFW window" << std::endl;
			glfwTerminate();
			return nullptr;
		}

		//��� ������ � OpenGL � Windows ������������ ������� ��������� ��������������� (rendering context), 
		//������� ��������� OpenGL � ������� �������� Windows. 
		//���� ������� �������� ���������� (device context) �������� ����������, 
		//����������� � ����������� ����������� GDI, �� �������� ��������������� �������� ����������, ����������� � OpenGL.
		//�� ������������� OPENGL API �� ������ ����� ������� OpenGL ��������
		//�������� ��������� ������� �� ��� ���, ���� �� �� ������� ������ �������� ������� ��� 
		//������� ������� �� ����� ������
		glfwMakeContextCurrent(window);
		int width, height;
		//��� ������ �� �������� OpenGL �������� �� ����� ��������� ������������ OpenGL
		//��� ����, ����� �������� ������� ��������, �� ������ ������������ �������� �������
		//GLFW ���� �� ��������� ������������ � ������� ������������.
		//��� ��������, ��� ���� �������� � ������ ������. �������� �����, ��� �� ��� 
		//����� ����������, ������ ����� ��� ����������� ����� (������� ������).
		//������� ����������� - ���� �� ���������� ������������ ������������� - ������ ������ � ��� ���������� screen tearing.
		//��� ������� ����������� � ��������� : ���� ���������� ������������ ������ ������ � �������, 
		//��� ����� ���������� �������(��������, ������� �������� �������� - 60 ����, 
		//�� ���� 60 ������ � �������, � ���������� ������ 100), �� ��������� ���� 
		//�������� ���������� �� ������� �� ��������� ��������� �����������
		//�����������, ������������ �����������, ������� �� ����� �� �����, 
		//� � ����������� �����(������� ������).��� ������ ������� �������� ��������� ����, 
		//���������� ����� ������ ��������� �� �������, � � ����� ������� ��������� ����.

		glfwGetFramebufferSize(window, &width, &height);
		//�� ������� ����� ����� ���� �� ����� ����������� ��� ����
		glViewport(0, 0, width, height);
		lastFrameTime = getCurrentTime();
		//glfwSwapInterval(FPS_CAP);
		return window;
	}
	//����������� ������ �����
	void DisplayManager::updateDisplay(GLFWwindow* window){
		//��������� �������
			glfwPollEvents();

			//����� ������ �� 1 �������
			fps++;

			int currentFrameTime = getCurrentTime();
		

	

				int deltaX = currentFrameTime - lastFrameTime;
				//������������ 33 �������������� �� ���� ����� ��� ��� 30 ������ � �������
				if (deltaX < 33)
				{
					//���� �������� �� ������� (����� ���������, ����� ��������� �������� ����)
					Sleep(33 - deltaX);

				}
				if (33 * fps >= 999)
				{ 
					//����� ������ � �������
					cout <<"FPS: "<< fps << endl;
					//���������� fps
					fps = 0;
				}
			
			    lastFrameTime = currentFrameTime;
			
			glfwSwapBuffers(window);
	
			delta = 33;
	}
	void DisplayManager::closeDisplay(){
		glfwTerminate();
	}

	int DisplayManager::getWidth()
	{
		return WIDTH;
	}

	int DisplayManager::getHeight()
	{
		return HEIGHT;
	}

	int DisplayManager::getFrameTimeSeconds()
	{
		return DisplayManager::delta;
	}

