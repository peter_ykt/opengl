#pragma once
#include "ModelData.h"
#include "Vertex.h"
#include <vector>
#include "libraries.h"
#include "Loader.h"
#include <sstream>
#include <fstream>
#include <string>
using namespace std;
class OBJFileLoader
{
public:
	static RawModel loadObj(string objFileName,Loader loader);
	static void processVertex(vector<string>vertex, vector<int>& indices, vector<glm::vec2>& textures, vector<glm::vec3>&normals, 
		float* textureArray, int textureArraySize, float* normalsArray, int normalArraySize);

private:
	static vector<string>split(const string& s, char delimiter);
	static int* convertIndicesListToArray(vector<int>indices, int& arraySize);
	static float convertDataToArrays(vector<Vertex> vertices, vector<glm::vec2>textures, vector<glm::vec3>normals, float* verticesArray, int verticesArraySize, 
		float* texturesArray, int texturesArraySize, float* normalsArray, int normalsArraySize);
	static void dealWithAlreadyProcessedVertex(Vertex* previousVertex, int newTextureIndex, int newNormalIndex,
		vector<int>&indices, vector<Vertex>vertices);
	static void removeUnusedVertices(vector<Vertex>vertices);

	OBJFileLoader();
	~OBJFileLoader();
};

