#include "EntityRenderer.h"

void EntityRenderer::prepareInstance(Entity entity)
{
	glm::mat4 transformationMatrix = Maths::createTransformationMatrix(entity.getPosition(), entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());
	shader.loadTransformationMatrix(transformationMatrix);
}

void EntityRenderer::unbindTexturedModel()
{
	enableCulling();
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

void EntityRenderer::prepareTexturedModel(TexturedModel model)
{
	RawModel rawModel = model.getRawModel();
	glBindVertexArray(rawModel.getVaoID());
	glEnableVertexAttribArray(0); // position
	glEnableVertexAttribArray(1); // textureCoordinates
	glEnableVertexAttribArray(2); // normal
	ModelTexture* texture = model.getTexture();
	
	if ((*texture).isHasTransparency()) {
		//��������� ���������� ���������� ������������
		disableCulling();
	}
	shader.loadFakeLightingVariable((*texture).isUseFakeLighting());
	shader.loadShineVariables((*texture).getShineDamper(), (*texture).getReflectivity());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, (*model.getTexture()).getID());

}

EntityRenderer::EntityRenderer()
{
}

EntityRenderer::EntityRenderer(StaticShader shader, glm::mat4 projectionMatrix)
{
	this->shader = shader;
	shader.start();
	shader.loadProjectionMatrix(projectionMatrix);
	shader.stop();
}

void EntityRenderer::render(map<TexturedModel, vector<Entity>> entities)
{
	//traverse - ����� �������
	for (auto it = entities.begin(); it != entities.end(); ++it)
	{
		//first ��� ���� � �������
		prepareTexturedModel(it->first);
		auto batch = it->second;



		for (int i = 0; i < batch.size(); i++)
		{
			prepareInstance(batch[i]);
			//��������� ������ ��������
			glDrawElements(GL_TRIANGLES, it->first.getRawModel().getVertexCount(), GL_UNSIGNED_INT, 0);

		}
		unbindTexturedModel();
	}
}

void EntityRenderer::enableCulling()
{
	//����� �� ���������� ���������� �����������, ����� �� �� ������������
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void EntityRenderer::disableCulling()
{
	glDisable(GL_CULL_FACE);
}