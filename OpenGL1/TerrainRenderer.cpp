#include "TerrainRenderer.h"

void TerrainRenderer::prepareTerrain(Terrain terrain)
{
	RawModel rawModel = terrain.getModel();
	glBindVertexArray(rawModel.getVaoID());
	glEnableVertexAttribArray(0);//���������� ������
	glEnableVertexAttribArray(1);//���������� ��������
	glEnableVertexAttribArray(2);//���������� ��������
	bindTextures(terrain);
	shader.loadShineVariables(1,0);
	glActiveTexture(GL_TEXTURE0);

}

void TerrainRenderer::bindTextures(Terrain terrain)
{
	TerrainTexturePack texturePack = terrain.getTexturePack();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texturePack.getBackgroundTexture().getTextureID());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texturePack.getrTexture().getTextureID());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texturePack.getgTexture().getTextureID());
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texturePack.getbTexture().getTextureID());
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, terrain.getBlendMap().getTextureID());
}

void TerrainRenderer::unbindTerrain()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

void TerrainRenderer::loadModelMatrix(Terrain terrain)
{
	glm::mat4 transformationMatrix = Maths::createTransformation(glm::vec3(terrain.getX(), 0, terrain.getZ()), 0, 0, 0, 1);//�� �������� �����������
	shader.loadTransformationMatrix(transformationMatrix);
}

TerrainRenderer::TerrainRenderer()
{
}

TerrainRenderer::TerrainRenderer(TerrainShader shader, glm::mat4 projectionMatrix)
{
	this->shader = shader;
	shader.start();
	shader.loadProjectionMatrix(projectionMatrix);
	shader.connectTextureUnits();
	shader.stop();
}

void TerrainRenderer::render(vector<Terrain> terrains)
{
	vector<Terrain>::iterator it;
	for (it = terrains.begin(); it != terrains.end(); ++it)
	{
		prepareTerrain(*it);
		loadModelMatrix(*it);
		//������ �����������
		glDrawElements(GL_TRIANGLES, (*it).getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
		unbindTerrain();
	}
}
