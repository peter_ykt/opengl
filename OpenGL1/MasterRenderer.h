#pragma once
#include "StaticShader.h"
#include "Renderer.h"
#include "TexturedModel.h"
#include <vector>
#include "Entity.h"
#include "Light.h"
#include "Camera.h"
#include "TerrainRenderer.h"
#include "TerrainShader.h"
#include "Terrain.h"
#include "EntityRenderer.h"
using namespace std;
	class MasterRenderer
	{
	private:
		const static float FOV;
		static const float NEAR_PLANE;
		static const float FAR_PLANE;
		static const float RED;
		static const float GREEN;
		static const float BLUE;
		glm::mat4 projectionMatrix;
		StaticShader shader = StaticShader();
		EntityRenderer renderer;
		TerrainRenderer terrainRenderer;
		TerrainShader terrainShader = TerrainShader();
		//������� ���� ������� ������� �� ����� � ������ Entity
		//�� ���� �� ������ ���� ���� ������ ��������
		map<TexturedModel, vector<Entity>> entities;
		vector<Terrain>terrains;
		void createProjectionMatrix();

	public:
		void render(Light sun, Camera camera);
		void processEntity(Entity entity);
		void cleanUp();
		void processTerrain(Terrain terrain);
		void prepare();
		MasterRenderer();



	};
