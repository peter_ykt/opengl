#include "TexturedModel.h"
int TexturedModel::idStatic = 0;

TexturedModel::TexturedModel()
{
	this->modelData = ModelData();
	this->texture = ModelTexture();
	idStatic++;
	idItem = idStatic;
}

TexturedModel::TexturedModel(RawModel model, ModelTexture texture)
{
	this->rawModel = model;
	this->texture = texture;
	idStatic++;
	idItem = idStatic;
}

TexturedModel::TexturedModel(ModelData model, ModelTexture texture)
{
	this->modelData = modelData;
	this->texture = texture;
	idStatic++;
	idItem = idStatic;
}

RawModel TexturedModel::getRawModel() const
{
	return this->rawModel;
}

ModelTexture* TexturedModel::getTexture()
{
	return &(this->texture);
}
int TexturedModel::getIdItem() const
{
	return idItem;
}
ModelData TexturedModel::getModelData() const
{
	return modelData;
}
//����� ����������� ��������� ��� ��� ������ � ������� map
//bool TexturedModel::operator==(TexturedModel & b)
bool TexturedModel::operator<(const TexturedModel & rhs) const
{
	return getIdItem() < rhs.getIdItem();

	
}

//{
//	if (this->modelData.getTextureCoords() == b.modelData.getTextureCoords()
//		&& this->modelData.getIndices() == b.modelData.getIndices()
//		&& this->modelData.getNormals() == b.modelData.getNormals()
//		&& this->modelData.getVertices() == b.modelData.getVertices())
//
//		return true;
//	else
//		return false;
//}




