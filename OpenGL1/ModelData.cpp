#include "ModelData.h"






ModelData::ModelData()
{
	this->vertices = nullptr;
	this->verticesSize = 0;
	this->textureCoords = nullptr;
	this->textureCoordsSize = 0;
	this->normals = nullptr;
	this->normalsSize = 0;
	this->indices = nullptr;
	this->indicesSize = 0;
	this->furthestPoint = 0.0f;
}

ModelData::ModelData(float* vertices, int verticesSize, float* textureCoords, int textureCoordsSize, float* normals, int normalsSize, int* indices, int indicesSize, float furthestPoint)
{
	this->vertices = vertices;
	this->verticesSize = verticesSize;
	this->textureCoords = textureCoords;
	this->textureCoordsSize = textureCoordsSize;
	this->normals = normals;
	this->normalsSize = normalsSize;
	this->indices = indices;
	this->indicesSize = indicesSize;
	this->furthestPoint = furthestPoint;

}

