#include "MasterRenderer.h"
const float MasterRenderer::FOV = 70.0f;
const float MasterRenderer::NEAR_PLANE = 0.1f;
const float MasterRenderer::FAR_PLANE = 1000.0f;
const float MasterRenderer::RED = 0.5f;
const float MasterRenderer::GREEN = 0.5f;
const float MasterRenderer::BLUE = 0.5f;


void MasterRenderer::createProjectionMatrix()
{
	//������� ������ ���� ����� ��� �����

	//GLint m_viewport[4];
	//glGetIntegerv(GL_VIEWPORT, m_viewport);
	////������
	//GLint width = m_viewport[2];
	////������
	//GLint height = m_viewport[3];
	//� ����� � ���
	GLint width = DisplayManager::getWidth();
	GLint height = DisplayManager::getHeight();
	float aspectRatio = (float)width / (float)height;
	float y_scale = (float)((1.0f / glm::tan(FOV / 2.0f)*aspectRatio));
	float x_scale = y_scale / aspectRatio;
	float frustrum_length = FAR_PLANE - NEAR_PLANE;
	projectionMatrix = glm::mat4();
	projectionMatrix[0][0] = x_scale;
	projectionMatrix[1][1] = y_scale;
	projectionMatrix[2][2] = -((FAR_PLANE - NEAR_PLANE) / frustrum_length);
	projectionMatrix[2][3] = -1;
	projectionMatrix[3][2] = -((2 * NEAR_PLANE*FAR_PLANE) / frustrum_length);
	projectionMatrix[3][3] = 0;

}

void MasterRenderer::render(Light sun, Camera camera)
{
	prepare();
	shader.start();
	shader.loadSkyColour(RED, GREEN, BLUE);
	shader.loadLight(sun);
	shader.loadViewMatrix(camera);
	renderer.render(entities);
	shader.stop();
	entities.clear();
	terrainShader.start();
	terrainShader.loadSkyColour(RED, GREEN, BLUE);
	terrainShader.loadLight(sun);
	terrainShader.loadViewMatrix(camera);
	terrainRenderer.render(terrains);
	terrainShader.stop();
	terrains.clear();
	entities.clear();
}

void MasterRenderer::processEntity(Entity entity)
{
	TexturedModel entityModel = entity.getModel();
	//����� �������� ������ ����� ������� �������� < � TexturedModel
	//bool TexturedModel::operator<(const TexturedModel & rhs) const
	//��� ����� ��� ������ �� �����
	map<TexturedModel, vector<Entity>>::iterator it = entities.find(entityModel);
	//���� ������� ���� (���� ���������� �� ������������ �������� ������ idStatic)
	if (it!= entities.end())
	{
		//���� ������ - ��������� � ������
		(*it).second.push_back(entity);
	}
	else
	{
		//����� ������� ����� ���� �� ������� - ��������� ������ ������� ������
		vector<Entity> newBatch;
		newBatch.push_back(entity);
		entities.insert(pair<TexturedModel,vector<Entity>>(entityModel, newBatch));

	}

	
}

void MasterRenderer::cleanUp()
{
	shader.cleanUp();
	terrainShader.cleanUp();
}

void MasterRenderer::prepare()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(RED, GREEN, BLUE, 1.0f);
}

void MasterRenderer::processTerrain(Terrain terrain)
{
	terrains.push_back(terrain);
}

MasterRenderer::MasterRenderer()
{
	//�������� ����������� ���������� ������������
	EntityRenderer::enableCulling();
	createProjectionMatrix();
	renderer = EntityRenderer(shader, projectionMatrix);
	terrainRenderer = TerrainRenderer(terrainShader, projectionMatrix);
}




//MasterRenderer::~MasterRenderer()
//{
//}
