#include "Terrain.h"
//������������� ������������ ��������
const float Terrain::SIZE = 800.0f;
const int Terrain::VERTEX_COUNT = 256;

RawModel Terrain::generateTerrain(Loader loader,const char * heightMap)
{
	//������ ����
	read_png_file(heightMap);
	int VERTEX_COUNT = height;
	heights = new float *[VERTEX_COUNT];
	for (int i = 0; i < VERTEX_COUNT; i++)
		heights[i] = new float[VERTEX_COUNT];
	int count = Terrain::VERTEX_COUNT * Terrain::VERTEX_COUNT;
	float* vertices = new float[count * 3];
	float* normals = new float[count * 3];
	float* textureCoords = new float[count * 2];
	int* indices = new int[6 * (Terrain::VERTEX_COUNT - 1)*(Terrain::VERTEX_COUNT * 1)];
	int vertexPointer = 0;
	for(int i=0;i< Terrain::VERTEX_COUNT;i++)
		for (int j = 0; j < Terrain::VERTEX_COUNT; j++)
			{
			//����� ���� ��������� ��������
			vertices[vertexPointer * 3] = (float)j / ((float)Terrain::VERTEX_COUNT - 1) * SIZE;
			float height = getHeight(j, i);
			heights[j][i] = height;
			vertices[vertexPointer * 3 + 1] = height;
			vertices[vertexPointer * 3 + 2] =(float)i / ((float)Terrain::VERTEX_COUNT - 1) * SIZE;
		
			glm::vec3 normal = calculateNormal(j, i);
			normals[vertexPointer * 3] = normal.x;
			normals[vertexPointer * 3 + 1] = normal.y;
			normals[vertexPointer * 3 + 2] = normal.z;
			textureCoords[vertexPointer * 2] = (float)j / ((float)Terrain::VERTEX_COUNT - 1);
			textureCoords[vertexPointer * 2 + 1] = (float)i / ((float)Terrain::VERTEX_COUNT - 1);
			vertexPointer++;
		}

	int pointer = 0;
	for (int gz = 0; gz < Terrain::VERTEX_COUNT - 1; gz++) {
		for (int gx = 0; gx < Terrain::VERTEX_COUNT - 1; gx++) {
			int topLeft = (gz*Terrain::VERTEX_COUNT) + gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz + 1)*Terrain::VERTEX_COUNT) + gx;
			int bottomRight = bottomLeft + 1;
			indices[pointer++] = topLeft;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = topRight;
			indices[pointer++] = topRight;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = bottomRight;
		}
	}
	return loader.loadToVAO(vertices, count*3, textureCoords,
		count*2, normals, count*3, indices, 6 * (Terrain::VERTEX_COUNT - 1)*(Terrain::VERTEX_COUNT * 1));
}

glm::vec3 Terrain::calculateNormal(int x, int z)
{
	float heightL = getHeight(x - 1, z);
	float heightR = getHeight(x + 1, z);
	float heightD = getHeight(x, z-1);
	float heightU = getHeight(x, z + 1);
	glm::vec3 normal = glm::vec3(heightL - heightR, 2.0f, heightD - heightU);
	return glm::normalize(normal);
}

Terrain::Terrain(int gridX, int gridZ, Loader loader, TerrainTexturePack texturePack, TerrainTexture blendMap,const char * heightMap)
{

	this->texturePack = texturePack;
	this->blendMap = blendMap;
	this->x = gridX * Terrain::SIZE;
	this->z = gridZ * Terrain::SIZE;
	this->model = generateTerrain(loader, heightMap);
}





float Terrain::getX()
{
	return x;
}

float Terrain::getZ()
{
	return z;
}

RawModel Terrain::getModel()
{
	return model;
}

TerrainTexturePack Terrain::getTexturePack()
{
	return texturePack;
}
TerrainTexture Terrain::getBlendMap()
{
	return blendMap;
}

void Terrain::read_png_file(const char * filename)
{
	FILE *fp = fopen(filename, "rb");

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png) abort();

	png_infop info = png_create_info_struct(png);
	if (!info) abort();

	if (setjmp(png_jmpbuf(png))) abort();

	png_init_io(png, fp);

	png_read_info(png, info);

	width = png_get_image_width(png, info);
	height = png_get_image_height(png, info);
	color_type = png_get_color_type(png, info);
	bit_depth = png_get_bit_depth(png, info);

	// Read any color_type into 8bit depth, RGBA format.
	// See http://www.libpng.org/pub/png/libpng-manual.txt

	if (bit_depth == 16)
		png_set_strip_16(png);

	if (color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_palette_to_rgb(png);

	// PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
	if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		png_set_expand_gray_1_2_4_to_8(png);

	if (png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);

	// These color_type don't have an alpha channel then fill it with 0xff.
	if (color_type == PNG_COLOR_TYPE_RGB ||
		color_type == PNG_COLOR_TYPE_GRAY ||
		color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

	if (color_type == PNG_COLOR_TYPE_GRAY ||
		color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png);

	png_read_update_info(png, info);

	if (row_pointers) abort();

	row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * height);
	for (int y = 0; y < height; y++) {
		row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png, info));
	}

	png_read_image(png, row_pointers);

	fclose(fp);

	png_destroy_read_struct(&png, &info, NULL);
}

float Terrain::getHeight(int x, int z)
{
	if (x < 0 || x >= height || z < 0 || z >= height)
		return 0.0f;
	png_bytep row = row_pointers[z];
	png_bytep px = &(row[x * 4]);

	
	float height =  (px[0]*256*256+px[1]*256+px[2])-256*256*256;
	height += MAX_PIXEL_COLOR / 2.0f;
	height /= MAX_PIXEL_COLOR / 2.0f;
	height *= MAX_HEIGHT;

	return height;
}

float Terrain::getHeightOfTerrain(float worldX, float worldZ)

	{
		float terrainX = worldX - this->x;
		float terrainZ = worldZ - this->z;
		int length = height;
		float gridSquareSize = SIZE / ((float)(length)-1);
		int gridX = floor(terrainX / gridSquareSize);
		int gridZ = floor(terrainZ / gridSquareSize);
		if (gridX >= length - 1 || gridZ >= length - 1 || gridX < 0 || gridZ < 0)
			return 0;
		float xCoord = fmodf(terrainX , gridSquareSize) / gridSquareSize;
		float zCoord = fmodf(terrainZ, gridSquareSize) / gridSquareSize;
		float answer;
		if (xCoord <= (1.0f - zCoord)) {
			answer = Maths::barryCentric(glm::vec3(0.0f, heights[gridX][gridZ], 0.0f), glm::vec3(1, heights[gridX + 1][gridZ], 0.0f),
				glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f), glm::vec2(xCoord, zCoord));
		}
		else {
			answer = Maths::barryCentric(glm::vec3(1.0f, heights[gridX + 1][gridZ], 0.0f), glm::vec3(1.0f, heights[gridX + 1][gridZ + 1], 1.0f),
				glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f), glm::vec2(xCoord, zCoord));
		}
		return answer;
	
}
