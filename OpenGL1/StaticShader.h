#pragma once
#include "ShaderProgram.h"
#include "GLM\glm.hpp"
#include "Camera.h"
#include "Maths.h"
#include "Light.h"
class StaticShader : public ShaderProgram {
private:
	static const char* VERTEX_FILE;
	static const char* FRAGMENT_FILE;
	int location_transformationMatrix;
	int location_projectionMatrix;
	int location_viewMatrix;
	int location_lightPosition;
	int location_lightColour;
	int location_shineDamper;
	int location_reflectivity;
	int location_useFakeLighting;
	int location_skyColour;
public:
	StaticShader();
	void loadTransformationMatrix(glm::mat4 matrix);
	void loadProjectionMatrix(glm::mat4 projection);
	void loadViewMatrix(Camera camera);
	void loadLight(Light light);
	void loadShineVariables(float damper, float reflectivity);
	void loadFakeLightingVariable(bool useFake);
	void loadSkyColour(float r, float g, float b);
	
	
	//οεπεξοπεδελενθε
protected:
	void bindAttributes();
    void getAllUniformLocations();
};