#pragma once
class TerrainTexture
{
private: 
	int textureID;
public:
	TerrainTexture();
	TerrainTexture(int textureID);
	int getTextureID();

};

