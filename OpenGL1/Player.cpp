#include "Player.h"
#include "Terrain.h"
const float Player::RUN_SPEED = 0.1f;
const float Player::TURN_SPEED = 0.05f;
const float Player::GRAVITY = -0.0018f;
const float Player::JUMP_POWER = 1.0f;
//const float Player::TERRAIN_HEIGHT = 0.0f;

//const float Player::TERRAIN_HEIGHT = 0.0f;

float Player::distance;
float Player::dx;
float Player::dz;

void Player::checkInputs()
{
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		this->currentSpeed = RUN_SPEED;
	else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		this->currentSpeed = -RUN_SPEED;
	else
		this->currentSpeed = 0.0f;

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		this->currentTurnSpeed = -TURN_SPEED;
	else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		this->currentTurnSpeed = TURN_SPEED;
	else
		this->currentTurnSpeed = 0.0f;
	if (glfwGetKey(window, GLFW_KEY_SPACE))
	{
		jump();
	}

}
	void Player::jump()
	{
		if (!isAirborn) {
			upwardsSpeed = JUMP_POWER;
			isAirborn = true;
		}
	}
	


	/*Player::Player()
	{
		cout << "player empty" << endl;
	}*/

	Player::Player(GLFWwindow * window, TexturedModel model, glm::vec3 position, float rotX, float rotY, float rotZ, float scale)
	:Entity(model, position, rotX, rotY, rotZ, scale)
{
	this->window = window;
	cout << "player not empty" << endl;
}

void Player::move(Terrain* terrain)
{
	checkInputs();
	increaseRotation(0, currentTurnSpeed*DisplayManager::getFrameTimeSeconds(), 0);
	
//	cout << DisplayManager::getFrameTimeSeconds() << endl;

	float distance = currentSpeed * DisplayManager::getFrameTimeSeconds();
	Player::distance = distance;
	//float distance = currentSpeed*5;
	float dx = distance * sin(getRotY());
	

	float dz = distance * cos(getRotY());
	Player::dx = dx;
	Player::dz = dz;
	increasePosition(dx, 0, dz);
//	cout << getPosition().x << endl;
	 upwardsSpeed += GRAVITY * DisplayManager::getFrameTimeSeconds();
	// cout << upwardsSpeed << endl;
	increasePosition(0, upwardsSpeed*DisplayManager::getFrameTimeSeconds()/10, 0);
	
	float terrainHeight = terrain->getHeightOfTerrain(getPosition().x, getPosition().z);
	
	/*if (getPosition().y < Player::TERRAIN_HEIGHT) {
		upwardsSpeed = 0;
		setPosition(glm::vec3(getPosition().x, Player::TERRAIN_HEIGHT, getPosition().z));
		isAirborn = false;
		
	}*/
	
	
	if (getPosition().y < terrainHeight) {
	upwardsSpeed = 0;
	setPosition(glm::vec3(getPosition().x, terrainHeight, getPosition().z));
	isAirborn = false;

}
}
