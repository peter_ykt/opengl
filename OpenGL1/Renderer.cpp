#include "Renderer.h"


void Renderer::createProjectionMatrix()
{
	//������� ������ ���� ����� ��� �����

	//GLint m_viewport[4];
	//glGetIntegerv(GL_VIEWPORT, m_viewport);
	////������
	//GLint width = m_viewport[2];
	////������
	//GLint height = m_viewport[3];
	//� ����� � ���
	GLint width = DisplayManager::getWidth();
	GLint height = DisplayManager::getHeight();
	float aspectRatio = (float)width / (float)height;
	float y_scale = (float)((1.0f / glm::tan(FOV / 2.0f)*aspectRatio));
	float x_scale = y_scale / aspectRatio;
	float frustrum_length = FAR_PLANE - NEAR_PLANE;
	projectionMatrix = glm::mat4();
	projectionMatrix[0][0] = x_scale;
	projectionMatrix[1][1] = y_scale;
	projectionMatrix[2][2] = -((FAR_PLANE - NEAR_PLANE)/frustrum_length);
	projectionMatrix[2][3] = -1;
	projectionMatrix[3][2] = -((2 * NEAR_PLANE*FAR_PLANE) / frustrum_length);
	projectionMatrix[3][3] = 0;
}

void Renderer::prepareInstance(Entity entity)
{
	glm::mat4 transformationMatrix = Maths::createTransformationMatrix(entity.getPosition(),
		entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());
	shader.loadTransformationMatrix(transformationMatrix);
}

void Renderer::unbindtexturedModel()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

void Renderer::cleanUp()
{
	unbindtexturedModel();
}



Renderer::Renderer(StaticShader shader)
{
	this->shader = shader;
	//��� ����� ����� ������ ������ �� ������������ �����������
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	createProjectionMatrix();
	//����� �������������� glUniform - �������� uniform ���������� � ��������� ��������� ����� ������������ glUseProgram()
	//��� ������ shader.start()
	shader.start();
	shader.loadProjectionMatrix(projectionMatrix);
	shader.stop();
}

void Renderer::prepare()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glClear( GL_DEPTH_BUFFER_BIT);

	//���� ����� ���������� ������ ��� ��� ����� FPS
	//
	//����� ���� ������
	glClearColor(0, 0, 1, 1);

}

void Renderer::render(map<TexturedModel, vector<Entity>> entities)
{

	
	//traverse - ����� �������
	for(auto it=entities.begin();it!=entities.end();++it)
	{	
		//first ��� ���� � �������
		prepareTexturedModel(it->first);
		auto batch = it->second;
	
		

		for (int i=0;i<batch.size();i++)
		{
			prepareInstance(batch[i]);
			glDrawElements(GL_TRIANGLES, it->first.getRawModel().getVertexCount(), GL_UNSIGNED_INT, 0);

		}
		unbindtexturedModel();
	}


}

void Renderer::prepareTexturedModel(TexturedModel model)
{
	RawModel rawModel = model.getRawModel();
	glBindVertexArray(rawModel.getVaoID());
	glEnableVertexAttribArray(0);//���������� ������
	glEnableVertexAttribArray(1);//���������� ��������
	glEnableVertexAttribArray(3);//���������� ��������
	ModelTexture* texture = model.getTexture();
	shader.loadShineVariables((*texture).getShineDamper(), (*texture).getReflectivity());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, (*model.getTexture()).getID());
}




void Renderer::render(Entity entity,StaticShader shader)
{
	

	TexturedModel model = entity.getModel();
	RawModel rawModel = model.getRawModel();
	//���������� ����� VAO �� ������ ����������
	glBindVertexArray(rawModel.getVaoID());
	//��������� ��������
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glm::mat4 transformationMatrix = Maths::createTransformation(entity.getPosition(), entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());

	shader.loadTransformationMatrix(transformationMatrix);
	ModelTexture texture = *(model.getTexture());
	shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, (model.getTexture())->getID());
	//������
	glDrawElements(GL_TRIANGLES, rawModel.getVertexCount(), GL_UNSIGNED_INT, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

