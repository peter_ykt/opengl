#include "RawModel.h"

RawModel::RawModel()
{
}

RawModel::RawModel(int vaoID, int vertexCount) {
	this->vaoID = vaoID;
	this->vertexCount = vertexCount;
}
GLuint RawModel::getVaoID() const {
	return this->vaoID;
}
int RawModel::getVertexCount() {
	return this->vertexCount;
}
